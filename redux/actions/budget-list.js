import * as BudgetListConstants from "../constants/budget-list";
import axios from "axios";

export function getCountries() {
  return async (dispatch) => {
    const response = await axios.get("/api/countries");
    console.log(response.data);
    dispatch({
      type: BudgetListConstants.BUDGET_LIST_GET_COUNTRIES,
      budget_list_get_countries: response.data,
    });
  };
}

export function getRows() {
  return async (dispatch) => {
    const response = await axios.get("/api/budgets");
    console.log(response.data);
    dispatch({
      type: BudgetListConstants.BUDGET_LIST_GET_ROWS,
      budget_list_get_rows: response.data,
    });
  };
}

export function filterArea(filterData) {
  return async () => {
    console.log(filterData);
  };
}

export function searchFilter(filterData) {
  return async () => {
    console.log(filterData);
  }
}

export function pushCountryData() {
  return async () => {
    console.log("CALL COUNTRY PUSH!");
    const response = await axios.post("/api/budgets");
    console.log(response);
  };
}

export function pushBudgetListData() {
  return async () => {
    console.log("CALL LIST PUSH!");
    const response = await axios.post("/api/budgets");
    console.log(response);
  };
}