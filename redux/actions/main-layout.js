import * as MainLayoutConst from "../constants/main-layout";
// import

export function handleTopBarChange(value) {
  return async (dispatch) => {
    dispatch({
      type: MainLayoutConst.TOP_NAV,
      tabValue: value,
    });
  };
}
export function handleSideBar(value) {
  return async (dispatch) => {
    dispatch({
      type: MainLayoutConst.SIDE_BAR,
      sideBarValue: value,
    });
  };
}
