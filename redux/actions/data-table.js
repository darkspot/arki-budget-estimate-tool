import * as DataTableConstants from "../constants/data-table";
// import * as DataTableConst from "../constants/data-table";
import axios from "axios";

// export function handleBreadCrumbsState(value) {
//   return async (dispatch) => {
//     dispatch({
//       type: DataTableConst.BREAD_CRUMBS_STEPS,
//       breadCrumbsSteps: value,
//     });
//   };
// }

// export function getDataTables() {
//   return async (dispatch) => {
//     const response = await axios.get("/api/data-table");
//     console.log(response);
//     dispatch({
//       type: DataTableConstants.GET_TABLES,
//       list: response.data,
//     });
//   };
// }

export function getAreaRanges(area_ranges) {
  return async (dispatch) => {
    const response = await axios.get("/api/data-table/area-ranges");
    console.log(response.data);
    dispatch({
      type: DataTableConstants.GET_AREA_RANGES,
      areaRanges: area_ranges,
    });
  };
}

export function getCriterias(criterias) {
  return async (dispatch) => {
    const response = await axios.get("/api/data-table/rate-criterias");
    console.log(response.data);
    dispatch({
      type: DataTableConstants.GET_RATE_CRITERIAS,
      criterias: response.data,
    });
  };
}

export function handleSideBar(value) {
  return async (dispatch) => {
    dispatch({
      type: DataTableConstants.SIDE_BAR,
      sideBarValue: value,
    });
  };
}
