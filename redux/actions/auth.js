import { microsoftProvider } from '../../utils/firebaseConfig';
import { LOGIN_ERROR, LOGIN_SUCCESS, LOGOUT_ERROR, LOGOUT_SUCCESS } from '../constants/auth';

export const handleLogin = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    try {
      const result = firebase.auth().signInWithPopup(microsoftProvider);
      dispatch({ type: LOGIN_SUCCESS });
    } catch (error) {
      console.error(error);
      dispatch({ type: LOGIN_ERROR });
    }
  }
}

export const handleLogout = () => {
  return async (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    try {
      await firebase.auth().signOut()
      dispatch({ type: LOGOUT_SUCCESS });
    } catch (error) {
      dispatch({ type: LOGOUT_ERROR, err });
    }
  }
}