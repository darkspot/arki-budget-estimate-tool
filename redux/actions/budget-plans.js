import * as BudgetPlanConstants from "../constants/budget-plans";
import _ from "lodash";
import axios from "axios";
import * as RateCriteriaService from "../../services/RateCriteriaService";
import * as AreaRangeService from "../../services/AreaRangeService";
import * as DataTableConstants from "../constants/data-table";

export function getBudgetPlans() {
  return async (dispatch) => {
    dispatch({
      type: BudgetPlanConstants.SET_PAGE_LOADING,
      loading: true,
    });
    const response = await axios.get("/api/budget-plans");
    console.log(response);

    dispatch({
      type: BudgetPlanConstants.GET_BUDGET_PLANS,
      list: response.data,
    });
    dispatch({
      type: BudgetPlanConstants.SET_PAGE_LOADING,
      loading: false,
    });

  };
}

export function setCurrentPlan(plan) {
  return async (dispatch) => {
    // const response = await axios.get("/api/budget-plans");
    // console.log(response);

    dispatch({
      type: DataTableConstants.GET_AREA_RANGES,
      areaRanges: plan.area_ranges,
    });
    dispatch({
      type: BudgetPlanConstants.SET_CURRENT_PLAN,
      currentPlan: plan,
    });
  };
}

export function setTableCellChange(data) {
  return async (dispatch) => {
    // const response = await axios.post("/api/budget-plans", data);
    // console.log(response);
    dispatch({
      type: BudgetPlanConstants.ON_EDIT_CHANGE_TABLE,
      dataOnEditChange: data,
    });
  };
}

export function clearDataCollections() {
  return async (dispatch) => {
    dispatch({
      type: BudgetPlanConstants.CLEAR_COLLECTION_DATA,
    });
  };
}

export function onEditBudgetPlan(boolean) {
  return async (dispatch) => {
    dispatch({
      type: BudgetPlanConstants.ON_EDIT_TABLE,
      onEdit: boolean,
    });
  };
}

export function onSaveDataToFirebase(currentRates, newRates, budgetPlanId) {
  return async (dispatch) => {
    // const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    _.map(newRates, (rate) => {
      currentRates[rate.id][rate.field] = rate.newValue; //mutate the currentRates in redux store
    });

    dispatch({
      //clear data collection after saving to firebase
      type: BudgetPlanConstants.CLEAR_COLLECTION_DATA,
    });

    await axios.put(`/api/budget-plans/${budgetPlanId}`, {
      rates: currentRates,
      location: budgetPlanId,
    });

    console.log(currentRates, newRates);
  };
}

/* Below is the test method */

export function sampleGetData() {
  return async (dispatch) => {
    const response = await axios.get("/api/budget-plan");

    console.log("get" + response);
  };
}

export function handleOpenModal(bool) {
  return (dispatch) => {
    dispatch({
      type: BudgetPlanConstants.BUDGET_PLAN_MODAL,
      budget_plan_modal: bool,
    });
  };
}

export const postCreatedBudget = (data) => async (dispatch) => {
  let rates = {}; // For Processing Default values for rates  area_ranges.push(firstDataOfAreaRanges);
  let area_ranges = AreaRangeService.createAreaRanges(data);
  let rateCriteriasData = await RateCriteriaService.getRateCriterias();

  let columns = {}; // For setting the needed columns
  for (let j = 0; j < area_ranges.length; j++) {
    columns[area_ranges[j].code] = 1;
  }

  rates = rateCriteriasData.reduce(
    (map, { code }) => ({ ...map, [code]: columns }),
    {}
  );

  // Deletes the used data before posting to firebase
  delete data.area_ranges_array;
  delete data.lowest;
  delete data.secondLowest;

  // debugger
  let newData = {
    ...data,
    area_ranges,
    rates,
    rate_criterias: rateCriteriasData,
  };
  console.log(newData);
  await axios.post(`/api/budget-plans`, newData);
  getBudgetPlans()(dispatch)
};

export function getCountries() {
  return async (dispatch) => {
    const response = await axios.get("/api/countries");
    dispatch({
      type: BudgetPlanConstants.BUDGET_PLAN_GET_COUNTRIES,
      budget_plan_get_countries: response.data,
    });
  };
}
