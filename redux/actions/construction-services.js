import * as ConstructionConstants from "../constants/construction-services";
import _ from "lodash";

// import

export function selectPlan(selectedPlan, currentData) {
  return async (dispatch, getState) => {
    // console.log("data: ", selectedPlan, currentData);
    const { dataTable } = getState().constructionServices;

    _.map(dataTable, (data, index) => {
      if (_.isEqual(currentData, data)) {
        _.update(data, "economical.selected", (bool) => !bool);
        _.update(data, "standard.selected", (bool) => !bool);
      }
    });

    dispatch({
      type: ConstructionConstants.SELECT_PLAN,
      dataTable: [...dataTable],
    });
  };
}

export function setNotes(inputText, isEconomical, currentObject) {
  return async (dispatch, getState) => {
    // console.log("state: ", getState());
    const { dataTable } = getState().constructionServices;

    _.map(dataTable, (data, index) => {
      if (_.isEqual(currentObject, data)) {
        if (isEconomical) {
          _.update(data, "economical.notes", () => inputText.notes);
        }

        if (!isEconomical) {
          _.update(data, "standard.notes", () => inputText.notes);
        }
      }
    });

    dispatch({
      type: ConstructionConstants.UPDATE_DATA,
      dataTable: [...dataTable],
    });

    // dispatch({
    //   type: MainLayoutConst.TOP_NAV,
    //   tabValue: value,
    // });
  };
}
// export function handleSideBar(value) {
//   return async (dispatch) => {
//     dispatch({
//       type: MainLayoutConst.SIDE_BAR,
//       sideBarValue: value,
//     });
//   };
// }
