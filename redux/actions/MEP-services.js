import * as MEPServicesConstants from "../constants/MEP-services";
import _ from "lodash";

export function selectPlan(selectedPlan, currentData) {
  return async (dispatch, getState) => {
    const { dataTable } = getState().MEPServices;

    _.map(dataTable, (data, index) => {
      if (_.isEqual(currentData, data)) {
        _.update(data, "economical.selected", (bool) => !bool);
        _.update(data, "standard.selected", (bool) => !bool);
      }
    });

    dispatch({
      type: MEPServicesConstants.SELECT_PLAN,
      dataTable: [...dataTable],
    });
  };
}

export function setNotes(inputText, isEconomical, currentObject) {
  return async (dispatch, getState) => {
    const { dataTable } = getState().MEPServices;

    _.map(dataTable, (data, index) => {
      if (_.isEqual(currentObject, data)) {
        if (isEconomical) {
          _.update(data, "economical.notes", () => inputText.notes);
        }

        if (!isEconomical) {
          _.update(data, "standard.notes", () => inputText.notes);
        }
      }
    });

    dispatch({
      type: MEPServicesConstants.UPDATE_DATA,
      dataTable: [...dataTable],
    });
  };
}
