import * as EstimatesConst from "../constants/estimate";

export function handleBreadCrumbsState(value) {
  return async (dispatch) => {
    dispatch({
      type: EstimatesConst.BREAD_CRUMBS_STEPS,
      breadCrumbsSteps: value,
    });
  };
}

