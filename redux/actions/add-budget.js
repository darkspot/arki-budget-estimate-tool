import axios from "axios";
import * as BudgetFormConstants from "../constants/add-budget";

export function getListOfSteps() {
  return async (dispatch) => {
    dispatch({
      type: BudgetFormConstants.GET_STEP_LIST,
    });
  };
}

export function setPrevStep(stepList, activeStep, currentStepObj) {
  return async (dispatch) => {
    currentStepObj.isCompleted = false;
    currentStepObj.isActive = false;
    stepList[activeStep] = currentStepObj;

    activeStep--;

    const prevStep = stepList[activeStep];
    prevStep.isCompleted = false;

    dispatch({
      type: BudgetFormConstants.SET_ACTIVE_STEP,
      activeStep: activeStep,
    });
    dispatch({
      type: BudgetFormConstants.SET_CURRENT_STEPS,
      currentSteps: prevStep,
    });
    dispatch({
      type: BudgetFormConstants.SET_STEPS_LIST,
      stepList: stepList,
    });

    dispatch({
      type: BudgetFormConstants.IS_LAST_STEP,
      isLastStep: false,
    });

    if (activeStep === 0) {
      dispatch({
        type: BudgetFormConstants.IS_FIRST_STEP,
        isFirstStep: true,
      });
    }
  };
}

export function setNextStep(stepList, activeStep, currentStepObj) {
  return async (dispatch) => {
    currentStepObj.isCompleted = true;
    stepList[activeStep] = currentStepObj;
    activeStep++;
    //get the next object
    const nextObject = stepList[activeStep];
    nextObject.isActive = true;

    dispatch({
      type: BudgetFormConstants.SET_ACTIVE_STEP,
      activeStep: activeStep,
    });
    dispatch({
      type: BudgetFormConstants.SET_CURRENT_STEPS,
      currentSteps: nextObject,
    });
    dispatch({
      type: BudgetFormConstants.SET_STEPS_LIST,
      stepList: stepList,
    });

    dispatch({
      type: BudgetFormConstants.IS_FIRST_STEP,
      isFirstStep: false,
    });

    if (activeStep === 3) {
      dispatch({
        type: BudgetFormConstants.IS_LAST_STEP,
        isLastStep: true,
      });
    }
  };
}

export function clickedSteps(stepObj, stepIndex) {
  return async (dispatch, getState) => {
    const { steps } = getState().addBudget;

    console.log("clicked! at redux", stepObj, stepIndex);

    // while (steps.length == 4) {
    //   steps[0].isCompleted = false;
    // }

    steps.map((stepData, index) => {
      if (stepIndex === index) {
        stepData.isCompleted = false;
      }
      if (index > stepIndex) {
        stepData.isCompleted = false;
        stepData.isActive = false;
      }
    });

    console.log(steps);

    dispatch({
      type: BudgetFormConstants.SET_STEPS_LIST,
      stepList: steps,
    });

    dispatch({
      type: BudgetFormConstants.SET_CURRENT_STEPS,
      currentSteps: steps[stepIndex],
    });

    dispatch({
      type: BudgetFormConstants.SET_ACTIVE_STEP,
      activeStep: stepIndex,
    });

    if (stepIndex === 0) {
      dispatch({
        type: BudgetFormConstants.IS_FIRST_STEP,
        isFirstStep: true,
      });
    } else {
      dispatch({
        type: BudgetFormConstants.IS_FIRST_STEP,
        isFirstStep: false,
      });
    }
    if (stepIndex === 3) {
      dispatch({
        type: BudgetFormConstants.IS_LAST_STEP,
        isLastStep: true,
      });
    } else {
      dispatch({
        type: BudgetFormConstants.IS_LAST_STEP,
        isLastStep: false,
      });
    }
  };
}

export function getLocationRates(location) {
  return async (dispatch) => {
    const response = await axios.get(`/api/budget-plans/${location}`);
    dispatch({
      type: BudgetFormConstants.GET_LOCATION_RATES,
      location_rates: response.data,
    });
  };
}

export function handleCloseGraphModal() {
  return async (dispatch) => {
    console.log("clicled closed");
    dispatch({
      type: BudgetFormConstants.HANDLE_CLOSE_GRAPH_MODAL,
      isGraphModalOpen: false,
      typeOfGraph: 0,
    });
  };
}

export function handleGraphModal(typeOfGraph) {
  return async (dispatch) => {
    dispatch({
      type: BudgetFormConstants.HANDLE_OPEN_GRAPH_MODAL,
      isGraphModalOpen: true,
      typeOfGraph: typeOfGraph,
    });
  };
}

// export function getListOfSteps() {
//   return async (dispatch) => {
//     dispatch({
//       type: BudgetFormConstants.GET_STEP_LIST,
//     });
//   };
// }

export function getLocations() {
  return async (dispatch) => {
    const response = await axios.get("/api/budget-plans/locations");
    dispatch({
      type: BudgetFormConstants.ADD_BUDGET_GET_LOCATIONS,
      add_budget_get_locations: response.data,
    });
  };
}

export function getBudgetPlanById(planId) {
  return async (dispatch) => {
    const response = await axios.get(`/api/budget-plans/${planId}`);
    dispatch({
      type: BudgetFormConstants.SET_SELECTED_BUDGET_PLAN,
      budgetPlan: response.data,
    });
  };
}

export function setDesignServiceData(data) {
  console.log(data);
  return async (dispatch) => {
    dispatch({
      type: BudgetFormConstants.SET_DESIGN_SERVICE_DATA,
      set_design_service_data: data,
    });
  };
}

export function createBudget(budget) {
  return async (dispatch,getState) => {
    const savedBudget = await axios.post('/api/budgets', { budget });
    dispatch({
      type: 'EXAMPLE',
      budget: savedBudget,
    });
  };
}

export function updateBudget(budget) {
  return async (dispatch) => {
    const savedBudget = await axios.put(`/api/budgets/${budget.id}`, { budget });
    dispatch({
      type: 'EXAMPLE',
      budget: savedBudget,
    });
  };
}
