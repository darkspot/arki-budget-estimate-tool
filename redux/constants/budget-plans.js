export const GET_BUDGET_PLANS = "BUDGET_PLANS/GET_BUDGET_PLANS";
export const SET_CURRENT_PLAN = "BUDGET_PLANS/SET_CURRENT_PLAN";
export const ON_EDIT_TABLE = "BUDGET_PLANS/ON_EDIT_TABLE";
export const ON_EDIT_CHANGE_TABLE = "BUDGET_PLANS/ON_EDIT_CHANGE_TABLE";
export const CLEAR_COLLECTION_DATA = "BUDGET_PLANS/CLEAR_COLLECTION_DATA";
export const ON_SAVE_DATA_IN_FIREBASE = "BUDGET_PLANS/ON_SAVE_DATA_IN_FIREBASE";
export const BUDGET_PLAN_MODAL = "BUDGET_PLAN_MODAL";
export const BUDGET_PLAN_GET_COUNTRIES = "BUDGET_PLAN_GET_COUNTRIES";
export const SET_PAGE_LOADING = "BUDGET_PLANS/SET_PAGE_LOADING";