import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";

import storage from "redux-persist/lib/storage";

import rootReducer from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { getFirebase } from 'react-redux-firebase';

const persistConfig = {
  key: "arki-root",
  storage,
  whitelist: ["mainLayout"], //reducer that needs to persist
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

// const initialState = {
//   todos: [{ id: 123, text: "hello", completed: false }],
//   dataTables: {
//     list: [],
//   },
// };

const store = createStore(
  persistedReducer,
  // initialState,
  composeWithDevTools(
    applyMiddleware(
      thunk.withExtraArgument({ getFirebase })
    )
  ),
);
let persistor = persistStore(store);

export { store as default, persistor };
