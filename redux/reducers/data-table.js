import * as DataTableConst from "../constants/data-table";

const defaultState = {
  // list: [],
  criterias: [],
  areaRanges: [],
};

export default function DataTableReducer(state = defaultState, action) {
  switch (action.type) {
    // case DataTableConst.GET_TABLES:
    //   return {
    //     ...state,
    //     list: action.list,
    //   };
    case DataTableConst.GET_AREA_RANGES:
      return {
        ...state,
        areaRanges: action.areaRanges,
      };
    case DataTableConst.GET_RATE_CRITERIAS:
      return {
        ...state,
        criterias: action.criterias,
      };
    default:
      return state;
  }
}
