import * as BudgetListConstants from "../constants/budget-list";

const defaultState = {
    budget_list_get_countries: [],
    budget_list_get_cities: [],
    budget_list_get_columns: [],
    budget_list_get_rows: [],
};

  export default function BudgetListReducer(state = defaultState, action) {
    switch (action.type) {
        
      case BudgetListConstants.BUDGET_LIST_GET_COUNTRIES:
        return {
            ...state,
            budget_list_get_countries: action.budget_list_get_countries,
        };

      case BudgetListConstants.BUDGET_LIST_GET_ROWS:
        return {
            ...state,
            budget_list_get_rows: action.budget_list_get_rows,
        };

      default:
        return state;
    }
  }
  