import * as addBudgetConstants from "../constants/add-budget";

const defaultState = {
  steps: [
    { label: "Project Details", isCompleted: false, isActive: true },
    { label: "Design Services", isCompleted: false, isActive: false },
    { label: "Construction Services", isCompleted: false, isActive: false },
    { label: "MEP Services", isCompleted: false, isActive: false },
  ],
  activeStep: 0,
  currentSteps: {
    label: "Project Details",
    isCompleted: false,
    isActive: true,
  },
  isLastStep: false,
  isFirstStep: true,
  isGraphModalOpen: false,
  add_budget_get_locations: [],
  set_design_service_data: {},
};

export default function AddBudgetReducer(state = defaultState, action) {
  switch (action.type) {
    case addBudgetConstants.GET_STEP_LIST:
      return {
        ...state,
        steps: defaultState.steps,
      };

    case addBudgetConstants.SET_STEPS_LIST:
      return {
        ...state,
        stepList: action.stepList,
      };

    case addBudgetConstants.SET_ACTIVE_STEP:
      return {
        ...state,
        activeStep: action.activeStep,
      };

    case addBudgetConstants.SET_CURRENT_STEPS:
      return {
        ...state,
        currentSteps: action.currentSteps,
      };

    // case addBudgetConstants.GET_LOCATION_RATES:
    //   return {
    //     ...state,
    //     location_rates: action.location_rates,
    //   };

    case addBudgetConstants.IS_LAST_STEP:
      return {
        ...state,
        isLastStep: action.isLastStep,
      };

    case addBudgetConstants.IS_FIRST_STEP:
      return {
        ...state,
        isFirstStep: action.isFirstStep,
      };

    case addBudgetConstants.HANDLE_CLOSE_GRAPH_MODAL:
      return {
        ...state,
        isGraphModalOpen: action.isGraphModalOpen,
        typeOfGraph: action.typeOfGraph,
      };

    case addBudgetConstants.HANDLE_OPEN_GRAPH_MODAL:
      return {
        ...state,
        isGraphModalOpen: action.isGraphModalOpen,
        typeOfGraph: action.typeOfGraph,
      };

    case addBudgetConstants.ADD_BUDGET_GET_LOCATIONS:
      return {
        ...state,
        add_budget_get_locations: action.add_budget_get_locations,
      };
    case addBudgetConstants.SET_DESIGN_SERVICE_DATA:
      return {
        ...state,
        set_design_service_data: action.set_design_service_data,
      };
    case addBudgetConstants.SET_SELECTED_BUDGET_PLAN:
      return {
        ...state,
        budgetPlan: action.budgetPlan,
      };

    default:
      return state;
  }
}
