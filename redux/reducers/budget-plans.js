import * as BudgetPlanConstants from "../constants/budget-plans";

const defaultState = {
  list: [],
  onEdit: false,
  dataOnEditChange: [],
  budget_plan_modal: false,
  budget_plan_get_countries: [],
  isPageLoading: false,
};

export default function BudgetPlanReducer(state = defaultState, action) {
  switch (action.type) {
    case BudgetPlanConstants.GET_BUDGET_PLANS:
      return {
        ...state,
        list: action.list,
        currentPlan: action.list[0], //to get the first plan to be displayed
      };
    
    case BudgetPlanConstants.ON_EDIT_TABLE:
      return {
        ...state,
        onEdit: action.onEdit,
      };

    case BudgetPlanConstants.ON_EDIT_CHANGE_TABLE:
      return {
        ...state,
        dataOnEditChange: [...state.dataOnEditChange, action.dataOnEditChange],
      };

    case BudgetPlanConstants.CLEAR_COLLECTION_DATA:
      return {
        ...state,
        dataOnEditChange: [],
      };
    
    case BudgetPlanConstants.SET_CURRENT_PLAN:
      return {
        ...state,
        currentPlan: action.currentPlan,
      };
    
    case BudgetPlanConstants.BUDGET_PLAN_MODAL:
      return {
        ...state,
        budget_plan_modal: action.budget_plan_modal,
      };
    
    case BudgetPlanConstants.BUDGET_PLAN_GET_COUNTRIES:
      return {
        ...state,
        budget_plan_get_countries: action.budget_plan_get_countries,
      };
    case BudgetPlanConstants.SET_PAGE_LOADING:
      return {
        ...state,
        isPageLoading: action.loading,
      };

    default:
      return state;
  }
}
