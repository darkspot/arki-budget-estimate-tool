import * as MainLayoutConst from "../constants/main-layout";

const defaultState = {
  tabValue: 1,
  sideBarValue: false
};

export default function MainLayoutReducer(state = defaultState, action) {
  switch (action.type) {
    case MainLayoutConst.TOP_NAV:
      return {
        ...state,
        tabValue: action.tabValue,
      };
    case MainLayoutConst.SIDE_BAR:
      return {
        ...state,
        sideBarValue: action.sideBarValue,
      };
    default:
      return state;
  }
}
