import * as ConstructionConstants from "../constants/construction-services";

const defaultState = {
  dataTable: [
    {
      name: "Partitions, Doors and Associated Works",
      code: "partitions",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Flooring",
      code: "flooring",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },

    {
      name: "Ceilings",
      code: "ceiling",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Wall Finishing",
      code: "wall-finishes",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Loose Furniture",
      code: "loose-furniture",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "System Furniture",
      code: "system-furniture",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Miscellaneous",
      code: "miscellaneous",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
  ],
};

export default function SampleReducer(state = defaultState, action) {
  switch (action.type) {
    case ConstructionConstants.SELECT_PLAN:
      return {
        ...state,
        dataTable: action.dataTable,
      };
    case ConstructionConstants.UPDATE_DATA:
      return {
        ...state,
        dataTable: action.dataTable,
      };
    default:
      return state;
  }
}
