import * as EstimatesConst from "../constants/estimate";

const defaultState = {
  breadCrumbsSteps: 1,
  estimateList:[
    {
      id: 1,
      project_name: "project ni John",
      location: "Manila City",
      price: 20000,
      sq_ft: 200,
    },
    {
      id: 2,
      project_name: "project ni Juan",
      location: "Quezon City",
      price: 20000,
      sq_ft: 200,
    },
    {
      id: 3,
      project_name: "project ni kel",
      location: "Rizal City",
      price: 20000,
      sq_ft: 200,
    },
  ]
};

export default function EstimateReducer(state = defaultState, action) {
  switch (action.type) {
    case EstimatesConst.BREAD_CRUMBS_STEPS:
      return {
        ...state,
        breadCrumbsSteps: action.breadCrumbsSteps,
      };
    default:
      return state;
  }
}
