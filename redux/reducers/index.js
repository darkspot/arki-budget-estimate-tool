import { combineReducers } from "redux";

import sampleReducer from "./sample";
import budgetPlans from "./budget-plans";
import dataTable from "./data-table";
import estimates from "./estimate";
import mainLayout from "./main-layout";
import budgetList from "./budget-list";
import addBudget from "./add-budget";
import MEPServices from "./MEP-services";
import constructionServices from "./construction-services";
import auth from "./auth";
import { firebaseReducer } from "react-redux-firebase";

export default combineReducers({
  auth,
  sampleReducer,
  mainLayout,
  dataTable,
  estimates,
  budgetPlans,
  budgetList,
  addBudget,
  MEPServices,
  constructionServices,
  firebase: firebaseReducer,
});
