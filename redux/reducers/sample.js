import * as Sample from "../constants/sample";

const defaultState = {
  backdrop: false,
};

export default function SampleReducer(state = defaultState, action) {
  switch (action.type) {
    case Sample.SAMPLE_TOGGLE:
      return {
        ...state,
        sample: true,
      };
    default:
      return state;
  }
}
