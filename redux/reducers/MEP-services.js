import * as MEPServicesConstants from "../constants/MEP-services";
const defaultState = {
  dataTable: [
    {
      name: "Electrical Services",
      code: "electrical-services",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Mechanical Services",
      code: "mechanical-services",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },

    {
      name: "Lighting",
      code: "lighting",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Fire System",
      code: "fire-system",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Plumbing Services",
      code: "plumbing-services",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "AV",
      code: "av",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "Security",
      code: "security",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
    {
      name: "It Infrastructure",
      code: "it-infrastructure",
      standard: {
        notes: "notes here",
        rates: 0,
        selected: true,
      },
      economical: {
        notes: "notes here",
        rates: 0,
        selected: false,
      },
    },
  ],
};

export default function SampleReducer(state = defaultState, action) {
  switch (action.type) {
    case MEPServicesConstants.SELECT_PLAN:
      return {
        ...state,
        dataTable: action.dataTable,
      };
    case MEPServicesConstants.UPDATE_DATA:
      return {
        ...state,
        dataTable: action.dataTable,
      };
    default:
      return state;
  }
}
