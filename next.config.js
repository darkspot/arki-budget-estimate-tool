// // const withCSS = require("@zeit/next-css");
// // const withLess = require("@zeit/next-less");
// // const withPlugins = require("next-compose-plugins");
// // const fs = require("fs");
// // const path = require("path");

// // const nextConfig = {
// //   env: {},
// // };

// // const plugins = [
// //   withLess({
// //     lessLoaderOptions: {
// //       javascriptEnabled: true,``
// //     },
// //     webpack: (config, { isServer }) => {
// //       // Fixes npm packages that depend on `fs` module
// //       config.node = {
// //         fs: "empty",
// //       };

// //       if (isServer) {
// //         const antStyles = /antd\/.*?\/style.*?/;
// //         const origExternals = [...config.externals];
// //         config.externals = [
// //           (context, request, callback) => {
// //             if (request.match(antStyles)) return callback();
// //             if (typeof origExternals[0] === "function") {
// //               origExternals[0](context, request, callback);
// //             } else {
// //               callback();
// //             }
// //           },
// //           ...(typeof origExternals[0] === "function" ? [] : origExternals),
// //         ];

// //         config.module.rules.unshift({
// //           test: antStyles,
// //           use: "null-loader",
// //         });
// //       }

// //       return config;
// //     },
// //   }),
// //   withCSS,
// // ];

// // module.exports = withPlugins(plugins, nextConfig);

// const withLess = require("@zeit/next-less");
// const withCSS = require("@zeit/next-css");

// module.exports = withLess({
//   lessLoaderOptions: {
//     javascriptEnabled: true,
//   },
// });

// // withLess({
// //   lessLoaderOptions: {
// //     javascriptEnabled: true,
// //   },
// // });

module.exports = {
    // Webpack 5 is enabled by default
    // You can still use webpack 4 while upgrading to the latest version of Next.js by adding the "webpack5: false" flag
    webpack5: false,
  }