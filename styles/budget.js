export const budgetSearchStyles = (theme) => ({
  title: {
    marginRight: "40px",
  },
  searchHolder: {
    marginTop: "40px",
  },
  showRatesBtn: {
    backgroundColor: "rgba(240, 242, 245, 1)",
    padding: "10.48px 14px",
  },
});
