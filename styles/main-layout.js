const drawerWidth = 240;
const mainStyles = (theme) => ({
  main: {
    width: "100%",
    height: "calc(100vh - 48px)",
    padding: "30px",
  },
  searchField: {
    "& div": {
      marginTop: 0,
    },
    "& label": {
      marginLeft: "10px",
    },
    "&:after": {
      borderBottom: `2px solid red`,
    },
  },
  mainContainer: {
    width: "calc(100vw - 67px)",
  },
  drawerClose: {
    width: 0,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    // ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  sideNavClose: {
    display: "none",
  },
  sideNav: {
    boxShadow:
      "0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 22px 0px rgb(0 0 0 / 12%)",
    display: "flex",
    justifyContent: "center",

    position: "relative",
    backgroundColor: "#FFF",
    width: "65px",
    // maxWidth: "65px",
    // minWidth: "50px",
  },
  hamburgerIcon: {
 
    marginTop: "10px",
    fontSize: "38px",
    // cursor: "pointer",
  },
  toolbar: {
    margin: "20px 0px",
  },
  
});

export default mainStyles;
