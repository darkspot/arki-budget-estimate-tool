export const addBudgetStyles = (theme) => ({
  PanelContainer: {
    padding: "20px 15px",
  },
});

export const addBudgetPanel = (theme) => ({
  headerBtn: {
    [theme.breakpoints.up("md")]: {
      padding: "12px 42px",
      height: "48px",

      marginTop: "8px",
      background: "#F8F9FE !important",
      borderRadius: "4px",
    },
    [theme.breakpoints.down("md")]: {
      padding: "12px 42px",
      height: "38px",

      marginTop: "8px",
      background: "#F8F9FE !important",
      borderRadius: "4px",
    },
  },
  headerTextBtn: {
    [theme.breakpoints.up("md")]: {
      fontWeight: "500",
      fontSize: "18px",
      lineheight: "24px",
    },
    [theme.breakpoints.down("md")]: {
      fontWeight: "500",
      fontSize: "16px",
      lineheight: "24px",
    },
  },
  inLineEditContainer: {
    "& span": {
      display: "inline-block",
      width: "100%",
    },
    "& textarea": {
      width: "100%",
      height: "200px",

      "&::-webkit-scrollbar": {
        width: "8px",
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "rgba(116, 89, 217, 0.2)",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#7459D9",
        borderRadius: "10px",
      },
      "&::-webkit-scrollbar-track:hover": {
        backgroundColor: "rgba(66, 47, 138, 0.4)",
      },
    },
  },
  boxIsEnabled: {
    color: "blue",
    padding: "10px",
  },
  boxIsDisabled: {
    color: "gray",
    padding: "10px",
  },
  noValuePlaceholder: {
    margin: "20px",
    height: "150px",
    backgroundColor: "aliceblue",
    color: "cornflowerblue",
    width: "calc(100% - 40px)",
    padding: "10px",
    fontStyle: "italic",
  },
  inlineTextField: {
    margin: "20px",
    width: "calc(100% - 40px)",
    height: "150px",
    overflowY: "auto",
  },
  inlineTextFieldValue: {
    margin: "20px",
    width: "calc(100% - 40px)",
  },
  titleField: {
    [theme.breakpoints.up("md")]: {
      fontWeight: "600",
      fontSize: "16px",
      lineHeight: "30px",
    },
    [theme.breakpoints.down("md")]: {
      fontWeight: "600",
      fontSize: "14px",
      lineHeight: "20px",
    },
  },

  paddingField: {
    padding: "0 20px",
  },
  header: {
    // border: "1px solid black",
  },
  headerTitle: {
    fontSize: "24px",
    fontWeight: "600",
    lineHeight: "30px",
    marginTop: "20px",
    color: theme.palette.secondary.main,
  },
  columnHeader: {
    textAlign: "center",
    padding: "0 10px",
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "24px",
    marginTop: "20px",
    color: theme.palette.secondary.main,
  },

  groupHeader: {
    fontSize: "16px",
    fontWeight: "600",
    lineHeight: "40px",
    color: theme.palette.secondary.main,
  },

  scrollBar: {
    "&::-webkit-scrollbar": {
      width: "12px",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "rgba(116, 89, 217, 0.2)",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#7459D9",
      borderRadius: "10px",
    },
    "&::-webkit-scrollbar-track:hover": {
      backgroundColor: "rgba(66, 47, 138, 0.4)",
    },
  },

  subtleBorderLine: {
    border: "1px solid #F0F2F5",
  },

  constructServicesHeader: {
    padding: "5px 10px",
    fontSize: "16px",
    fontWeight: "bolder",
    color: "rgba(66, 47, 138, 0.87)",
    textTransform: "uppercase",
    width: "300px",
  },

  rowBottomBorder: {
    borderBottom: "1px solid #F0F2F5",
  },
  // marginTop: {
  //   marginTop: "40px",
  // },
});
// [theme.breakpoints.up('md')]:{},
//     [theme.breakpoints.down('md')]:{}
export const estimatesPanel = (theme) => ({
  total: {
    color: "#D0D1D2",
    fontWeight: "bold",

    [theme.breakpoints.up("md")]: {
      fontSize: "36px",
    },
    [theme.breakpoints.down("md")]: {
      fontSize: "24px",
    },
  },
  avatar: {
    [theme.breakpoints.up("md")]: {
      height: "35px",
      width: "35px",
      marginRight: "10px",
      fontSize: "16px",
    },
    [theme.breakpoints.down("md")]: {
      height: "25px",
      width: "25px",
      marginRight: "10px",
      fontSize: "12px",
    },
  },

  HistoryContainer: {
    padding: "30px 0px 30px 20px",
    // marginBottom: "20px",
    marginTop: "20px",
    borderRadius: "20px",
    backgroundColor: theme.palette.primary.secondary,
    overflow: "hidden",
  },
  HistoryList: {
    overflow: "auto",
    width: "100%",
    [theme.breakpoints.up("md")]: {
      minHeight: "100px",
      maxHeight: "300px",
    },
    [theme.breakpoints.down("md")]: {
      minHeight: "100px",
      maxHeight: "200px",
    },
  },

  historyCloseBtn: {
    borderRadius: "5px",
    height: "30px",
    width: "30px",
    backgroundColor: theme.palette.secondary.main,
    cursor: "pointer",
  },
  smallGrapContainer: {
    marginBottom: "20px",
    padding: "10px",
    // marginTop: "40px",
    borderRadius: "20px",
    backgroundColor: theme.palette.primary.secondary,
    height: "250px",
    width: "100%",
    cursor: "pointer",
  },

  bigGraphContainer: {
    outline: "none",
    marginBottom: "20px",
    padding: "30px",
    borderRadius: "20px",
    backgroundColor: theme.palette.primary.secondary,
    height: "550px",
    width: "800px",
    // margin: "0 auto",
    // position: "absolute",
    // top: "50%",

    // cursor: "pointer",
  },

  scrollBar: {
    "&::-webkit-scrollbar": {
      width: "6px",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "rgba(116, 89, 217, 0.2)",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#7459D9",
      borderRadius: "10px",
    },
    "&::-webkit-scrollbar-track:hover": {
      backgroundColor: "rgba(66, 47, 138, 0.4)",
    },
  },
});
