import _ from "lodash";
import millify from "millify";

const createMaxAreaRangeValue = (lastMaxValue) => {
  return {
    code: `${millify(lastMaxValue)}+`,
    name: `${millify(lastMaxValue)}+`,
    min_value: lastMaxValue,
  };
}

const createAreaRangeValue = (lowest, highest) => {
  return {
    code: `${millify(lowest)}-${millify(highest)}`,
    name: `${millify(lowest)} - ${millify(highest)}`,
    min_value: parseInt(lowest),
    max_value: parseInt(highest),
  }
}

export const createAreaRanges = (data) => {
  let area_ranges = [];
  let firstValue = createAreaRangeValue(data.lowest, data.secondLowest);
  area_ranges.push(firstValue);

  if (!data.area_ranges_array) {
    data.area_ranges_array = [];
  }

  let lastFormValue = _.last(data.area_ranges_array);

  if (!lastFormValue) {
    const lastValue = createMaxAreaRangeValue(firstValue.max_value);
    area_ranges.push(lastValue);
  }

  if (!_.isEmpty(data.area_ranges_array && lastFormValue)) {
    const areaRanges = _.map(data.area_ranges_array, (rawAreaRange) => {
      const newAreaRange = createAreaRangeValue(rawAreaRange.min, rawAreaRange.max);
      return newAreaRange;
    });
    area_ranges.push(...areaRanges);

    const lastRawValue = _.last(data.area_ranges_array);
    const lastValue = createMaxAreaRangeValue(lastRawValue.max);
    area_ranges.push(lastValue);
  }
  return area_ranges;
}