import _ from "lodash";

class RateGetter {
  getRatesByArea(budgetPlan, area) {
    // console.log("this is budget plane and area @RateGetter:", budgetPlan, area);
    const matchedArea = this.getAreaRangesObj(
      budgetPlan.area_ranges,
      parseInt(area)
    );

    return this.getRates(budgetPlan.rates, matchedArea);
  }

  getAreaRangesObj(areaRanges, area) {
    const matchedArea = _.find(areaRanges, (obj) => {
      if (_.inRange(area, obj.min_value, obj.max_value)) {
        return true;
      }
    });

    if (matchedArea) {
      return matchedArea;
    } else {
      return _.last(areaRanges);
    }
  }

  getRates(rates, matchedArea) {
    const newFilter = _.mapValues(rates, (obj) => {
      return obj[matchedArea.code];
    });

    return newFilter; //<-- object
  }
}

export default RateGetter;
