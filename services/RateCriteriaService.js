import axios from "axios";

export const getRateCriterias = async () => {
  const response = await axios.get(`/api/rate-criterias`);
  return response.data;
};
