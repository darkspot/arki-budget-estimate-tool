class DataTableProcess {
  dataTable = [];
  prevCriterias = "";

  getProcessedDataTable(ratesGivenByArea, rateCriterias) {
    let categoryObj = {};
    let data_row = [];

    rateCriterias.forEach((criteria, index) => {
      if (this.prevCriterias.length === 0) {
        categoryObj.category = criteria.category;
        this.prevCriterias = criteria.category;
      }

      if (this.prevCriterias !== criteria.category && !!index) {
        this.dataTable.push({ ...categoryObj, data_row: data_row }); //push the obj
        categoryObj = {}; //reset the object
        data_row = []; //reset the data_row
        categoryObj.category = criteria.category;
        this.prevCriterias = criteria.category;
      }
      data_row.push(this.getDataRows(ratesGivenByArea, criteria));
    });
    console.log("this is data table:", this.dataTable);
    return this.dataTable;
  }

  getDataRows(ratesGivenByArea, criteria) {
    // title: "Architecture",
    // category: "Design Services",
    // is_required: true,
    // typical_fl_rates: 12, //firebase -<
    // base_line: 12,
    return {
      title: criteria.name,
      category: criteria.category,
      is_required: true, //set into true first for test
      typical_fl_rates: ratesGivenByArea[criteria.code],
      base_line: ratesGivenByArea[criteria.code], //<- baseline is disame as typical rates, becaues by default typical 100% and uplift 0
    };
  }
}

export default DataTableProcess;
