import _ from "lodash";

class TableRateCriteriasSort {

    setOrderedSubDesignServices = (data) => {
        _.map(data["Design Services"], (obj) => {
        if(obj.id == "architecture"){
            obj.order_id = 1;
        }
        else if(obj.id == "interior-design"){
            obj.order_id = 2;
        }else if(obj.id == "engineering"){
            obj.order_id = 3;
        }
        });

        data["Design Services"] = _.orderBy(data["Design Services"], ['order_id'], ['asc']);
        return data["Design Services"];
    }

    setOrderedSubBuildingAssessment = (data) => {
        _.map(data["Building Assessment"], (obj) => {
        if(obj.id == "due-diligence-multi"){
            obj.order_id = 1;
        }
        else if(obj.id == "due-diligence-single"){
            obj.order_id = 2;
        }else if(obj.id == "due-diligence-retrofit"){
            obj.order_id = 3;
        }
        });

        data["Building Assessment"] = _.orderBy(data["Building Assessment"], ['order_id'], ['asc']);
        return data["Building Assessment"];
    }

    setOrderedSubStrategy = (data) => {
        _.map(data["Strategy"], (obj) => {
        if(obj.id == "WES"){
            obj.order_id = 1;
        }else if(obj.id == "strategy-complete"){
            obj.order_id = 2;
        }else if(obj.id == "strategy-light"){
            obj.order_id = 3;
        }else if(obj.id == "brief-development"){
            obj.order_id = 4;
        }else if(obj.id == "change-strategy"){
            obj.order_id = 5;
        }else if(obj.id == "digital-workplace-strategy"){
            obj.order_id = 6;
        }else if(obj.id == "pilot-user-demo"){
            obj.order_id = 7;
        }else if(obj.id == "changed-management"){
            obj.order_id = 8;
        }
        });

        data["Strategy"] = _.orderBy(data["Strategy"], ['order_id'], ['asc']);
        return data["Strategy"];
    }

    setOrderedSubProgramming = (data) => {
        _.map(data["Programming"], (obj) => {
        if(obj.id == "testfit-services"){
            obj.order_id = 1;
        }
        else if(obj.id == "feasibility-study"){
            obj.order_id = 2;
        }else if(obj.id == "master-planning"){
            obj.order_id = 3;
        }else if(obj.id == "environment-branding-strategy"){
            obj.order_id = 4;
        }
        });

        data["Programming"] = _.orderBy(data["Programming"], ['order_id'], ['asc']);
        return data["Programming"];
    }

    setOrderedSubCodeAndEnvironment = (data) => {
        _.map(data["Code and Environment"], (obj) => {
        if(obj.id == "leed-assessment"){
            obj.order_id = 1;
        }
        else if(obj.id == "well-assessment"){
            obj.order_id = 2;
        }else if(obj.id == "code-review"){
            obj.order_id = 3;
        }
        });

        data["Code and Environment"] = _.orderBy(data["Code and Environment"], ['order_id'], ['asc']);
        return data["Code and Environment"];
    }

    setOrderedSubProjectLeadership = (data) => {
        _.map(data["Project Leadership"], (obj) => {
        if(obj.id == "lead-consultant"){
            obj.order_id = 1;
        }
        else if(obj.id == "other-consultants"){
            obj.order_id = 2;
        }else if(obj.id == "budget-estimate"){
            obj.order_id = 3;
        }else if(obj.id == "project-schedule"){
            obj.order_id = 4;
        }
        });

        data["Project Leadership"] = _.orderBy(data["Project Leadership"], ['order_id'], ['asc']);
        return data["Project Leadership"];
    }

    setOrderDesignServices = (data) => {
        let orderedGroups = {
        'Design Services': data["Design Services"],
        'Building Assessment': data["Building Assessment"],
        'Strategy': data["Strategy"],
        'Programming': data["Programming"],
        'Code and Environment': data["Code and Environment"],
        "Project Leadership": data["Project Leadership"],
        "Preliminaries": data["Preliminaries"],
        };

        orderedGroups["Design Services"] = this.setOrderedSubDesignServices(orderedGroups);
        orderedGroups["Building Assessment"] = this.setOrderedSubBuildingAssessment(orderedGroups);
        orderedGroups["Strategy"] = this.setOrderedSubStrategy(orderedGroups);
        orderedGroups["Programming"] = this.setOrderedSubProgramming(orderedGroups);
        orderedGroups["Code and Environment"] = this.setOrderedSubCodeAndEnvironment(orderedGroups);
        orderedGroups["Project Leadership"] = this.setOrderedSubProjectLeadership(orderedGroups);

        return orderedGroups;
    }

    
  setOrderConstructionServices = (data) => {
    let orderedConstructionServices = [];

    orderedConstructionServices.push(data[2]);
    orderedConstructionServices.push(data[5]);
    orderedConstructionServices.push(data[6]);
    orderedConstructionServices.push(data[0]);
    orderedConstructionServices.push(data[4]);
    orderedConstructionServices.push(data[1]);
    orderedConstructionServices.push(data[3]);

    return orderedConstructionServices;
  }

  setOrderMEPServices = (data) => {
    let orderedMEPServices = [];

    orderedMEPServices.push(data[6]);
    orderedMEPServices.push(data[2]);
    orderedMEPServices.push(data[3]);
    orderedMEPServices.push(data[5]);
    orderedMEPServices.push(data[1]);
    orderedMEPServices.push(data[7]);
    orderedMEPServices.push(data[0]);
    orderedMEPServices.push(data[4]);

    return orderedMEPServices;
  }
}

export default TableRateCriteriasSort;