import React, { Component } from "react";

//components
import MainLayout from "../../components/layouts/Main";
import AddBudget from "../../components/add-budget";

import { connect } from "react-redux";

class AddBudgetPage extends Component {
  state = {};
  // componentDidMount() {}
  render() {
    return (
      <MainLayout noPadding>
        <AddBudget />
      </MainLayout>
    );
  }
}

const mapStateToProps = (state) => ({
  // steps: state.estimates.breadCrumbsSteps,
});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AddBudgetPage);
