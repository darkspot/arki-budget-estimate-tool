import React, { Component } from "react";
//next
import { withRouter } from "next/router";
//components
import MainLayout from "../../components/layouts/Main";
import DataListCard from "../../components/data-table/DataListCard";
//MUI
import { Button, Grid, Typography } from "@material-ui/core";
//redux & actions
import { connect } from "react-redux";
import * as budgetPlanActions from "../../redux/actions/budget-plans";
// import Login from "../../components/authentication/Login";

/*
  WARNING THIS COMPONENT IS NOT USED. IF YOU WANT TO EDIT THE BUDGET-PLAN PAGE
  WE SHIFTED IT INTO HOME PAGE.
*/

class BudgetPlanPage extends Component {
  render() {
    return (
      <MainLayout>
        <Grid container alignItems="center" spacing={3}>
          <Grid item>
            <Typography variant="h1">Capex Rate Matrix</Typography>
          </Grid>
          <Grid item>
            {(
              <Button variant="contained" color="secondary">
                Add Rate Matrix
              </Button>
            ) && false}
          </Grid>
        </Grid>
        <DataListCard />
      </MainLayout>
    );
  }
}

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({
  sampleGet: async () => {
    return dispatch(budgetPlanActions.sampleGetData());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(BudgetPlanPage));
