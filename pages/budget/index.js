import React, { Component } from "react";

//components
import MainLayout from "../../components/layouts/Main";
// import BudgetSearch from "../../components/budget/BudgetSearch";
// import Estimates from "../../components/estimates";
import BudgetList from "../../components/budget-list";

//redux & actions
import { connect } from "react-redux";

class BudgetPage extends Component {
  // state = {  }

  render() {
    return (
      <MainLayout>
        <BudgetList />
      </MainLayout>
    );
  }
}

const mapStateToProps = (state) => ({
  // sideBarValue: state.mainLayout.sideBarValue,
  // tabValue: state.mainLayout.tabValue,
});
const mapDispatchToProps = (dispatch) => ({
  // handleSideBar: (value) => {
  //   return dispatch(MainLayOutActions.handleSideBar(value));
  // },
});

export default connect(mapStateToProps, mapDispatchToProps)(BudgetPage);
