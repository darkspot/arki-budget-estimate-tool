import React, { Component } from "react";

//redux & actions
import { connect } from "react-redux";
import axios from "axios";
//components
import MainLayout from "../../components/layouts/Main";
import AddBudget from "../../components/add-budget";

class BudgetPage extends Component {
  state = {
    budget: null,
  };

  static async getInitialProps({ query }) {
    return { budgetId: query.budgetId };
  }

  async componentDidMount() {
    const response = await axios.get(`/api/budgets/${this.props.budgetId}`);
    this.setState({ budget: response.data });
  }

  render() {
    return (
      <MainLayout noPadding>
        {this.state.budget && <AddBudget budget={this.state.budget} />}
      </MainLayout>
    );
  }
}
const mapStateToProps = (state) => ({
  steps: state.estimates.breadCrumbsSteps,
});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(BudgetPage);
