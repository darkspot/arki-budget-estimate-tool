import React, { Component } from "react";

//next
import { withRouter } from "next/router";

//components
import DataListCard from "../components/data-table/DataListCard";
import MainLayout from "../components/layouts/Main";

//MUI
import { Button, Grid, Typography } from "@material-ui/core";

//redux & actions
import { connect } from "react-redux";
import * as budgetPlanActions from "../redux/actions/budget-plans";
import CreateBudgetPlanModal from "../components/budget-plans/CreateBudgetPlanModal";

// Final Form
class BudgetPlanPage extends Component {
  openModal = (data) => {
    this.props.handleOpenModal(data);
  };

  render() {
    return (
      <MainLayout>
        <Grid container spacing={3} lg={12} md={12} sm={12} xs={12}>
          <Grid item md={3} sm={3} xs={3}>
            <Typography variant="h1" style={{ marginTop: 12 }}>
              Capex Rate Matrix
            </Typography>
          </Grid>
          <Grid item md={9} sm={9} xs={9}>
            <Button
              variant="contained"
              className="bp-component-capex-button"
              disableElevation
              onClick={() => this.openModal(true)}
            >
              Create Rate Matrix
            </Button>
            <CreateBudgetPlanModal />
          </Grid>
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <DataListCard />
          </Grid>
        </Grid>
      </MainLayout>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  sampleGet: async () => {
    return dispatch(budgetPlanActions.sampleGetData());
  },
  handleOpenModal: async (bool) => {
    return dispatch(budgetPlanActions.handleOpenModal(bool));
  },
  postCreatedBudget: async (data) => {
    return dispatch(budgetPlanActions.postCreatedBudget(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(BudgetPlanPage));
