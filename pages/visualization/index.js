import React, { Component } from "react";
//components
import MainLayout from "../../components/layouts/Main";
//redux & actions
import { connect } from "react-redux";

class DataVisualizationPage extends Component {
  state = {};
  render() {
    return (
      <MainLayout>
        <div>this is the data visualization</div>
      </MainLayout>
    );
  }
}

export default DataVisualizationPage;
