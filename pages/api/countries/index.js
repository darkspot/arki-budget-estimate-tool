import * as  CountriesController from '../../../controllers/CountriesController';

export default async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return await CountriesController.getCountries(res);
    default:
      return res.status(404).send();
  }
}