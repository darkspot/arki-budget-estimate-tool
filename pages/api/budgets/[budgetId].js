import * as  BudgetController from '../../../controllers/BudgetController';

export default async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return await BudgetController.getBudgetById(req, res);
    case "POST":
      return await BudgetController.createBudget(req, res);
    case "PUT":
      return await BudgetController.updateBudget(req, res);
    case "DELETE":
      return await BudgetController.deleteBudget(req, res);
    default:
      return res.status(404).send();
  }
}