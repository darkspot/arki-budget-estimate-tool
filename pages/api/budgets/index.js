import * as  BudgetController from '../../../controllers/BudgetController';

export default async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return await BudgetController.getBudgetList(res);
    case "POST":
      return await BudgetController.createBudget(req, res);
    default:
      return res.status(404).send();
  }
}