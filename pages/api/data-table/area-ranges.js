import * as constants from "../../../constants/";
import db from "../../../utils/db";

const budgetPlanRef = db.collection("budget_plans");

export default async function handler(req, res) {
  if (req.method === "GET") {
        try {
        const allEntries = await budgetPlanRef.get();  // Get all the data in firebase based on Data Set
        res.status(200).json(allEntries.docs.reverse().map((doc) => doc.data()));
      } catch (e) {
        res.status(400).end();
      }
  }

  if (req.method === "POST") {
  }
  if (req.method === "PUT") {
  }
  if (req.method === "DELETE") {
  }
}
