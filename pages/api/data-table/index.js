export default function handler(req, res) {
  const datatables = [
    { location: "Manila" },
    { location: "Chicago" },
    { location: "London" },
  ]
  res.status(200).json(datatables)
}