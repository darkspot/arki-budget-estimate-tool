import * as  BudgetPlanController from '../../../controllers/BudgetPlanController';

export default async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return await BudgetPlanController.getBudgetPlanById(req, res);
    case "POST":
      return await BudgetPlanController.createBudgetPlan(req, res);
    case "PUT":
      return await BudgetPlanController.updateBudget(req, res);
    case "DELETE":
      return await BudgetPlanController.deleteBudget(req, res);
    default:
      return res.status(404).send();
  }
}