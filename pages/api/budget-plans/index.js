import * as  BudgetPlanController from '../../../controllers/BudgetPlanController';

export default async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return await BudgetPlanController.getBudgetPlans(res);
    case "POST":
      return await BudgetPlanController.createBudgetPlan(req, res);
    default:
      return res.status(404).send();
  }
}
