import * as  RateCriteriaController from '../../../controllers/RateCriteriaController';

export default async function handler(req, res) {
  switch (req.method) {
    case "GET":
      return await RateCriteriaController.getAvailableRateCriterias(res);
    default:
      return res.status(404).send();
  }
}