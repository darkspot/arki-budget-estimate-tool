import db from "../../../utils/db";

export default async function handler(req, res) {
  try {
    const body = req.body;
    if (req.method == "POST") {
      const entries = await db.collection("sample").doc("user_4").set(body); //.set() will post new and update if there's exisitng the same document name
      console.log("entries" + entries);
      res.status(200).json(entries);
    }
    if (req.method == "GET") {
      //get all
      const allEntries = await db.collection("sample").get();
      // .get(); //.set() will post new and update if there's exisitng the same document name
      // console.log("entries" + entries);
      res.status(200).json(allEntries.docs.map((doc) => doc.data())); //return all
    }
  } catch (e) {
    console.log("Error: " + e);
    res.status(400).end();
  }
}
