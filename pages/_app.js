import Head from "next/head";
import { Provider, useSelector } from "react-redux";
import { SnackbarProvider } from "notistack";
import LoaderContextProvider from "../components/layouts/LoaderContext";
import store from "../redux/store";
import firebaseConfig from "../utils/firebaseConfig";
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';

import "../styles/globals.css";
import "../styles/budget-list-styles.css";
import "../styles/modal-style.css";
import "../styles/budget-plan.css";
import "../styles/project-details.css";
import { isLoaded, ReactReduxFirebaseProvider } from "react-redux-firebase";
import { Backdrop, CircularProgress } from "@material-ui/core";

const rrfProps = {
  firebase: firebaseConfig,
  config: {}, // for firestore
  dispatch: store.dispatch,
};

function AuthIsLoaded({ children }) {
  const auth = useSelector((state) => state.firebase.auth);
  if (!isLoaded(auth))
    return (
      <Backdrop open={true}>
        <CircularProgress />
      </Backdrop>
    );
  return children;
}

const cache = createCache({ key: 'css' });
cache.compat = true;

function MyApp({ Component, pageProps }) {
  return (
    <CacheProvider value={cache}>
      <Head>
        <title>Arki tool</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <SnackbarProvider maxSnack={3}>
        <LoaderContextProvider>
          <Provider store={store}>
            <ReactReduxFirebaseProvider {...rrfProps}>
              <AuthIsLoaded>
                <Component {...pageProps} />
              </AuthIsLoaded>
            </ReactReduxFirebaseProvider>
          </Provider>
        </LoaderContextProvider>
      </SnackbarProvider>
    </CacheProvider>
  );
}

export default MyApp;
