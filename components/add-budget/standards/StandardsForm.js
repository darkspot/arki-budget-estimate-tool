import React, { Component } from "react";
import {
  Grid,
  Select,
  MenuItem,
  Typography,
  withStyles,
} from "@material-ui/core";
import * as addBudgetStyles from "../../../styles/add-budget";

import { Field, Form, FormSpy } from "react-final-form";
import BudgetContext from "../BudgetContext";

class StandardsForm extends Component {
  state = {};
  static contextType = BudgetContext;

  handleSubmit = (form) => {
    this.context.setBudget(form.values);
    console.log(form.values);
  };

  renderForm(formProps) {
    return (
      <form onSubmit={formProps.handleSubmit}>
        <FormSpy subscription={{ values: true }} onChange={this.handleSubmit} />
        <Grid container item md={12} style={{ marginTop: "20px" }}>
          <Grid item md={3} sm={3} style={{ padding: "0 10px" }}>
            <Typography variant="h6">WELL Standard</Typography>
            <Field
              name="well_standard"
              render={(fieldProps) => (
                <Select
                  style={{ width: "100%" }}
                  name="well"
                  variant="outlined"
                  value={fieldProps.input.value}
                  onChange={fieldProps.input.onChange}
                >
                  <MenuItem value={`n/a`}>Not Applicable</MenuItem>
                  <MenuItem value={`silver`}>Silver</MenuItem>
                  <MenuItem value={`gold`}>Gold</MenuItem>
                  <MenuItem value={`platinum`}>Platinum</MenuItem>
                </Select>
              )}
            />
          </Grid>
          <Grid item md={3} sm={3} style={{ padding: "0 10px" }}>
            <Typography variant="h6">LEED Standard</Typography>
            <Field
              name="leed_standard"
              render={(fieldProps) => (
                <Select
                  style={{ width: "100%" }}
                  name="well"
                  variant="outlined"
                  value={fieldProps.input.value}
                  onChange={fieldProps.input.onChange}
                >
                  <MenuItem value={`n/a`}>Not Applicable</MenuItem>
                  <MenuItem value={`certified`}>Certified</MenuItem>
                  <MenuItem value={`silver`}>Silver</MenuItem>
                  <MenuItem value={`gold`}>Gold</MenuItem>
                  <MenuItem value={`platinum`}>Platinum</MenuItem>
                </Select>
              )}
            />
          </Grid>
        </Grid>
      </form>
    );
  }

  render() {
    return (
      <Form
        onSubmit={this.handleSubmit}
        initialValues={this.context.budget || {}}
        render={this.renderForm.bind(this)}
      />
    );
  }
}

export default withStyles(addBudgetStyles.addBudgetPanel)(StandardsForm);
