import React, { Component } from "react";

import MEPServiceTable from "../../data-table/MEPServiceTable";
import StandardsForm from "../standards/StandardsForm";

class MEPServices extends Component {
  render() {
    return (
      <>
        <StandardsForm />
        <MEPServiceTable />
      </>
    );
  }
}

export default MEPServices;
