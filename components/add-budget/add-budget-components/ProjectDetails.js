import React, { Component } from "react";
import {
  Grid,
  OutlinedInput,
  withStyles,
  Select,
  MenuItem,
  TextField,
  CircularProgress,
} from "@material-ui/core";
import { Form, Field, FormSpy } from "react-final-form";
import { connect } from "react-redux";
import NumberFormat from "react-number-format";

import * as addBudgetStyles from "../../../styles/add-budget";
import * as addBudgetActions from "../../../redux/actions/add-budget";
import Container from "@material-ui/core/Container";

import BudgetContext from "../BudgetContext";

class ProjectDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static contextType = BudgetContext;

  componentDidMount = async () => {
    await this.props.getLocations();
  };

  handleChangeBudgetPlan = (fieldProps) => {
    return async (event) => {
      try {
        fieldProps.input.onChange(event);
        this.setState({ loading: true });
        const changedBudgetPlan = event.target.value;
        await this.context.props.getBudgetPlanById(changedBudgetPlan);
      } finally {
        this.setState({ loading: false });
      }
    };
  };

  handleFormChange = async (formProps) => {
    this.context.setBudget(formProps.values);
  };

  handleSubmit = (dataForm) => {
    this.context.setBudget(dataForm);
  };

  renderForms = (formProps) => {
    const { locations } = this.props;

    const { handleSubmit } = formProps;

    return (
      <form id="project_details" onSubmit={handleSubmit}>
        <FormSpy
          subscription={{ values: true }}
          onChange={this.handleFormChange}
        />
        <Container maxWidth="md">
          <Grid
            container
            spacing={6}
            md={12}
            sm={12}
            xs={12}
            // className="pd-component-padding-grid pd-component-padding-grid-additional-margin"
          >
            <Grid item md={9} sm={9} xs={7}>
              <p className="pd-component-margin-labels">Client Name</p>
              <Field
                name="client_name"
                render={({ meta, ...fieldProps }) => (
                  <OutlinedInput
                    value={fieldProps.input.value}
                    onChange={(event) => fieldProps.input.onChange(event)}
                    placeholder="Last name, First name"
                    style={{ width: "100%" }}
                  />
                )}
              />
            </Grid>
            <Grid item md={3} sm={3} xs={5}>
              <p className="pd-component-margin-labels">Total Area (sqft)</p>
              <Field
                name="area"
                render={({ meta, ...fieldProps }) => (
                  <NumberFormat
                    {...fieldProps}
                    value={fieldProps.input.value}
                    customInput={TextField}
                    type="text"
                    suffix=" sqft"
                    variant="outlined"
                    thousandSeparator
                    onValueChange={({ value: v }) =>
                      fieldProps.input.onChange({ target: { name, value: v } })
                    }
                    inputProps={{
                      style: { textAlign: "right" },
                    }}
                  />
                )}
              />
            </Grid>

            <Grid item md={4} sm={4} xs={11}>
              <p className="pd-component-margin-labels">Location </p>
              <Field
                name="location"
                render={(fieldProps) => <input type="hidden" {...fieldProps} />}
              />
              <Field
                name="budget_plan"
                render={({ meta, ...fieldProps }) => (
                  <Select
                    className="pd-component-select"
                    value={fieldProps.input.value}
                    onChange={this.handleChangeBudgetPlan(fieldProps)}
                    variant="outlined"
                    displayEmpty
                    // renderValue={(value) => value || "Select Location"}
                  >
                    {locations.map((location, index) => (
                      <MenuItem key={index} value={location.id}>
                        {location.location}
                      </MenuItem>
                    ))}
                  </Select>
                )}
              />
            </Grid>
            <Grid item md={5} sm={5} xs={7}>
              <p className="pd-component-margin-labels">Address</p>
              <Field
                name="address"
                render={({ meta, ...fieldProps }) => (
                  <OutlinedInput
                    value={fieldProps.input.value}
                    onChange={(e) => fieldProps.input.onChange(e)}
                    placeholder="Address"
                    style={{ width: "100%" }}
                  />
                )}
              />
            </Grid>
            <Grid item md={3} sm={3} xs={4}>
              <p className="pd-component-margin-labels">Currency</p>
              <p className="pd-component-currency-text">
                {this.state.loading && (
                  <CircularProgress color="secondary" size={15} />
                )}
                {!this.state.loading && this.context.getCurrency()}
              </p>
            </Grid>

            <Grid item md={9} sm={9} xs={7}>
              <p className="pd-component-margin-labels">Project Title</p>
              <Field
                name="project_title"
                render={({ meta, ...fieldProps }) => (
                  <OutlinedInput
                    value={fieldProps.input.value}
                    onChange={(e) => fieldProps.input.onChange(e)}
                    placeholder="Enter Title"
                    style={{ width: "100%" }}
                  />
                )}
              />
            </Grid>
            <Grid item md={3} sm={3} xs={4}>
              <p item className="pd-component-margin-labels">
                Project Type
              </p>
              <Field
                name="build"
                render={({ meta, ...fieldProps }) => (
                  <Select
                    className="pd-component-select"
                    value={fieldProps.input.value}
                    onChange={fieldProps.input.onChange}
                    variant="outlined"
                    displayEmpty
                    renderValue={(value) => value || "Select"}
                  >
                    <MenuItem value="New Build">New Build</MenuItem>
                    <MenuItem value="Renovation">Renovation</MenuItem>
                  </Select>
                )}
              />
            </Grid>
          </Grid>
        </Container>
      </form>
    );
  };

  render() {
    return (
      <Form
        initialValues={this.context.budget}
        subscription={{
          pristine: true,
          submitting: true,
          values: true,
        }}
        onSubmit={this.handleSubmit}
        render={(formProps) => this.renderForms(formProps)}
      />
    );
  }
}

const mapStateToProps = (state) => ({
  locations: state.addBudget.add_budget_get_locations,
});

const mapDispatchToProps = (dispatch) => ({
  getLocations: () => {
    return dispatch(addBudgetActions.getLocations());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(addBudgetStyles.addBudgetPanel)(ProjectDetails));
