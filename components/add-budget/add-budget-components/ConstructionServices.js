import React, { Component } from "react";
import {
  withStyles,
} from "@material-ui/core";
import * as addBudgetStyles from "../../../styles/add-budget";

import ConstructionServiceTable from "../../data-table/ConstructionServicesTable";
import StandardsForm from "../standards/StandardsForm";

class ConstructionServices extends Component {
  render() {
    return (
      <>
        <StandardsForm />
        <ConstructionServiceTable />
      </>
    );
  }
}

export default withStyles(addBudgetStyles.addBudgetPanel)(ConstructionServices);
