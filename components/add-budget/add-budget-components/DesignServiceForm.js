import React, { Component } from "react";

//MUI
import { withStyles } from "@material-ui/core";
import * as addBudgetStyles from "../../../styles/add-budget";
///redux
import { connect } from "react-redux";
import * as addBudgetActions from "../../../redux/actions/add-budget";
//components
import DesignServicesTable from "../design-services/DesignServicesTable";

import BudgetContext from "../BudgetContext";
import DesignFloorAreaForm from "../design-services/DesignFloorAreaForm";

import RateGetter from "../../../services/RateGetter";

class DesignServiceForm extends Component {
  static contextType = BudgetContext;

  state = {
    typical: 100,
    nonTypical: 0,
    uplift: 0,
  };

  handleChangeTypical = async (event) => {
    const { setTypicalFloorRates, setNonTypicalFloorRates } = this.context;
    let baseValue = 100;

    if (!(event.target.value < 0) && !(event.target.value > 100)) {
      setTypicalFloorRates(event.target.value);
      const { typical_rate } = this.context.state.budget;

      if (typical_rate > 100) {
        setTypicalFloorRates(100);
      } else if (typical_rate < 0) {
        setTypicalFloorRates(0);
      }

      let result = baseValue - typical_rate;
      setNonTypicalFloorRates(result);
    }
  };

  handleChangeNonTypical = async (event) => {
    const { setTypicalFloorRates, setNonTypicalFloorRates } = this.context;
    let baseValue = 100;

    if (!(event.target.value < 0) && !(event.target.value > 100)) {
      setNonTypicalFloorRates(event.target.value);
      const { non_typical_rate } = this.context.state.budget;

      if (non_typical_rate > 100) {
        setNonTypicalFloorRates(100);
      } else if (non_typical_rate < 0) {
        setNonTypicalFloorRates(0);
      }

      let result = baseValue - non_typical_rate;
      setTypicalFloorRates(result);
    }
  };

  handleChangeUplift = async (event) => {
    await this.setState({ uplift: event.target.value });
    if (this.state.uplift < 0) {
      await this.setState({ uplift: 0 });
    }
  };

  handleUpliftBlur = async () => {
    let designServiceData = {
      typical: this.state.typical,
      "non-typical": this.state.nonTypical,
      uplift: this.state.uplift,
    };

    await this.props.setDesignServiceData(designServiceData);
  };

  componentDidMount() {
    const ratesGivenByArea = new RateGetter().getRatesByArea(
      this.context.budgetPlan,
      this.context.budget.area
    );

    this.context.setRatesGivenByArea(ratesGivenByArea);
  }

  render() {
    return (
      <>
        <DesignFloorAreaForm />
        <DesignServicesTable />
      </>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch) => ({
  setDesignServiceData: async (data) => {
    return await dispatch(addBudgetActions.setDesignServiceData(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(addBudgetStyles.addBudgetPanel)(DesignServiceForm));
