import React from "react";

//MUI
import {
  withStyles,
  Grid,
  OutlinedInput,
  TextField,
  Typography,
} from "@material-ui/core";
import * as addBudgetStyles from "../../../styles/add-budget";
///redux
import { connect } from "react-redux";
import * as addBudgetActions from "../../../redux/actions/add-budget";
//components

import BudgetContext from "../BudgetContext";
import NumberFormat from "react-number-format";

class DesignFloorAreaForm extends React.Component {
  static contextType = BudgetContext;
  state = {
    typical: 100,
    nonTypical: 0,
    uplift: 0,
  };

  handleChangeTypical = async (event) => {
    const { setTypicalFloorRates, setNonTypicalFloorRates } = this.context;
    let baseValue = 100;

    if (!(event.target.value < 0) && !(event.target.value > 100)) {
      setTypicalFloorRates(event.target.value);
      const { typical_rate } = this.context.state.budget;

      if (typical_rate > 100) {
        setTypicalFloorRates(100);
      } else if (typical_rate < 0) {
        setTypicalFloorRates(0);
      }

      let result = baseValue - typical_rate;
      setNonTypicalFloorRates(result);
    }
  };

  handleChangeNonTypical = async (event) => {
    const { setTypicalFloorRates, setNonTypicalFloorRates } = this.context;
    let baseValue = 100;

    if (!(event.target.value < 0) && !(event.target.value > 100)) {
      setNonTypicalFloorRates(event.target.value);
      const { non_typical_rate } = this.context.state.budget;

      if (non_typical_rate > 100) {
        setNonTypicalFloorRates(100);
      } else if (non_typical_rate < 0) {
        setNonTypicalFloorRates(0);
      }

      let result = baseValue - non_typical_rate;
      setTypicalFloorRates(result);
    }
  };

  handleChangeUplift = (event) => {
    this.context.setUplift(event.target.value);
  };

  handleUpliftBlur = async () => {
    let designServiceData = {
      typical: this.state.typical,
      "non-typical": this.state.nonTypical,
      uplift: this.state.uplift,
    };

    await this.props.setDesignServiceData(designServiceData);
    // console.log("TYPICAL: ", this.state.typical, "NON-TYPICAL: ", this.state.nonTypical, "UPLIFT: ", this.state.uplift);
  };

  render() {
    const { classes } = this.props;
    const { typical_rate, non_typical_rate } = this.context.state.budget;
    return (
      <Grid
        container
        md={12}
        sm={12}
        alignContent="flex-start"
        style={{ marginTop: "20px" }}
      >
        <Grid item md={4} sm={4} style={{ padding: "0 20px" }}>
          <Typography className={classes.titleField} item>
            Typical Floor Area
          </Typography>
          <OutlinedInput
            value={
              _.has(this.context.state.budget, "typical_rate")
                ? typical_rate
                : 100
            }
            type="number"
            name="typical"
            onChange={this.handleChangeTypical}
            placeholder="0% - 100%"
            style={{ width: "100%" }}
          />
        </Grid>

        <Grid item md={4} sm={4} style={{ padding: "0 20px" }}>
          <Typography className={classes.titleField}>
            Non-Typical Floor Area
          </Typography>
          <OutlinedInput
            value={
              _.has(this.context.state.budget, "non_typical_rate")
                ? non_typical_rate
                : 0
            }
            type="number"
            name="non-typical"
            onChange={this.handleChangeNonTypical}
            placeholder="0% - 100%"
            style={{ width: "100%" }}
          />
        </Grid>

        <Grid
          className={classes.titleField}
          item
          md={4}
          sm={4}
          style={{ padding: "0 20px" }}
        >
          <Typography className={classes.titleField}>
            Non-Typical Floor Area Uplift
          </Typography>
          <NumberFormat
            value={this.context.state.budget.uplift_rate || 0}
            customInput={TextField}
            min={0}
            suffix="%"
            variant="outlined"
            thousandSeparator
            onValueChange={({ value: v }) =>
              this.handleChangeUplift({ target: { name, value: v } })
            }
            inputProps={{
              style: { textAlign: "right" },
            }}
          />
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = () => ({});
const mapDispatchToProps = (dispatch) => ({
  getLocationRates: async (location) => {
    return await dispatch(addBudgetActions.getLocationRates(location));
  },
  setDesignServiceData: async (data) => {
    return await dispatch(addBudgetActions.setDesignServiceData(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(addBudgetStyles.addBudgetPanel)(DesignFloorAreaForm));
