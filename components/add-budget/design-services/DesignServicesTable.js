import React, { Component } from "react";
import { withStyles, Grid, Switch, Typography, Container } from "@material-ui/core";
import clsx from "clsx";
import { Field, Form, FormSpy } from "react-final-form";
// Components
import * as addBudgetStyles from "../../../styles/add-budget";
import BudgetContext from "../BudgetContext";

import { RIEInput } from "riek";

import _ from "lodash";

class DesignServicesTable extends Component {
  static contextType = BudgetContext;

  handleSubmit = () => {
    console.log("hello");
  };

  handleChange = (formProps) => {
    if (this.context.state.budget.rate_by_area) {
      this.context.setBudget({
        ...formProps.values,
        rate_by_area: this.context.state.budget.rate_by_area,
      });
    }
  };

  handleRequiredField = (event, dataRow) => {
    // debugger;
    this.context.handleRequired(dataRow, event.target.checked);
  };

  handleInlineEdit = (props, onChange) => {
    this.context.setNewPreliminariesValue(props.preliminaries);
    onChange(props.preliminaries);
  };

  renderGroupedRates = (formProps, groupRates) => {
    return _.map(groupRates, (rateCriteria, rateCriteriaIndex) => {
      const criteriaPath = `rates.${rateCriteria.category}.${rateCriteria["sub-category"]}.${rateCriteria.code}`;
      return (
        <Grid container item md={12} sm={12} key={rateCriteriaIndex}>
          <Grid
            container
            alignItems="center"
            item
            md={4} sm={4}
            style={{ fontWeight: 600 }}
          >
            {rateCriteria.label}
          </Grid>

          {criteriaPath !==
          "rates.Construction Services.Preliminaries.preliminaries" ? (
            <>
              <Grid container justify="center" item md={2} sm={2}>
                <Field
                  name={`${criteriaPath}.enabled`}
                  render={(fieldProps) => (
                    <Switch
                      checked={Boolean(fieldProps.input.value)}
                      onChange={(event) => fieldProps.input.onChange(event)}
                    />
                  )}
                />
              </Grid>
              <Grid container justify="center" item md={3} sm={3}>
                {_.get(formProps.values, `${criteriaPath}.enabled`) &&
                  this.context.getTypicalRateByCriteria(
                    rateCriteria,
                    formProps.values
                  )}
              </Grid>
              <Grid container justify="center" item md={3} sm={3}>
                <Field
                  name={`${criteriaPath}.rate`}
                  render={(fieldProps) => (
                    <>
                      {_.get(formProps.values, `${criteriaPath}.enabled`) &&
                        this.context.getBaselineRateByCriteria(
                          rateCriteria,
                          formProps.values
                        )}
                      <input
                        type="hidden"
                        {...fieldProps}
                        value={this.context.getBaselineRateByCriteria(
                          rateCriteria,
                          formProps.values
                        )}
                      />
                    </>
                  )}
                />
              </Grid>
            </>
          ) : (
            <>
              <Grid container justify="center" item md={2} sm={3}>
                <Field
                  name={`${criteriaPath}.rate`}
                  render={(fieldProps) => (
                    <>
                      {/* {
                        // _.get(formProps.values, `${criteriaPath}.enabled`) &&
                        this.context.getBaselineRateByCriteria(
                          rateCriteria,
                          formProps.values
                        )
                      } */}

                      <RIEInput
                        propName="preliminaries"
                        value={this.context.getBaselineRateByCriteria(
                          rateCriteria,
                          formProps.values
                        )}
                        change={(inputText) =>
                          this.handleInlineEdit(
                            inputText,
                            fieldProps.input.onChange
                          )
                        }
                      />
                      
                    </>
                  )}
                />
              </Grid>
            </>
          )}
        </Grid>
      );
    });
  };

  renderForm(formProps) {
    const { classes } = this.props;
    const rateGroup = this.context.getDesignServicesRateGroup();
    console.log("context(state): ", this.context.state);
    return (
      <form onSubmit={formProps.handleSubmit}>
        <FormSpy subscription={{ values: true }} onChange={this.handleChange} />
        <Container maxWidth="lg">
          <Grid item>
            <Grid container item md={12} sm={12} className={clsx(classes.header)}>
              <Grid item md={4} sm={4} className={clsx(classes.headerTitle)}>
                Services
              </Grid>
              <Grid item md={2} sm={2} className={clsx(classes.columnHeader)}>
                Is this required?
              </Grid>
              <Grid item md={3} sm={3} className={clsx(classes.columnHeader)}>
                Typical Floor Rates
              </Grid>
              <Grid item md={3} sm={3} className={clsx(classes.columnHeader)}>
                Baseline Budget per (sqft)
              </Grid>
            </Grid>
          </Grid>
          {/* body table below */}
          <Grid
            className={clsx(classes.scrollBar)}
            item
            style={{
              overflow: "auto",
              maxHeight: "600px",
              height: "calc(100vh - 500px)",
            }}
          >
            {_.map(rateGroup, (groupRates, subCategory, index) => (
              <>
                <Grid item md={12} key={index}>
                  <Typography variant="h6" color="secondary">
                    {subCategory.toUpperCase()}
                  </Typography>
                </Grid>
                {this.renderGroupedRates(formProps, groupRates)}
              </>
            ))}

            {/* per row component */}
          </Grid>
        </Container>
      </form>
    );
  }

  render() {
    return (
      <Form
        initialValues={this.context.budget}
        onSubmit={this.handleSubmit}
        render={(formProps) => this.renderForm(formProps)}
      />
    );
  }
}

export default withStyles(addBudgetStyles.addBudgetPanel)(DesignServicesTable);
