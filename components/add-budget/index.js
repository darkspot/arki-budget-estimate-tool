import React, { Component } from "react";

//MUI
import { withStyles, Grid, Modal, Paper } from "@material-ui/core";
//styles
import * as addBudgetStyles from "../../styles/add-budget";
//compent
import BudgetFormPanel from "./BudgetFormPanel";
import TotalEstimatePanel from "./TotalEstimate";
import { BudgetContextProvider } from "./BudgetContext";

class AddBudget extends Component {
  state = {};

  handleClose = () => {
    console.log("closed");
  };

  render() {
    const { classes, budget } = this.props;
    console.log("this is the budget", budget);
    return (
      <BudgetContextProvider budget={budget}>
        <Grid
          container
          style={{ height: "100%", overflowY: "auto", overflowX: "hidden" }}
        >
          <Grid
            item
            lg={9}
            md={9}
            sm={9}
            xs={9}
            className={classes.PanelContainer}
          >
            <BudgetFormPanel budget={budget} />
          </Grid>
          <Grid
            lg={3}
            md={3}
            sm={3}
            xs={3}
            item
            className={classes.PanelContainer}
            style={{ backgroundColor: "#EEEEEE" }}
          >
            <TotalEstimatePanel budget={budget} />
          </Grid>
        </Grid>
      </BudgetContextProvider>
    );
  }
}

export default withStyles(addBudgetStyles.addBudgetStyles)(AddBudget);
