import React, { Component } from "react";
import { Grid, Typography, withStyles, Avatar } from "@material-ui/core";

import * as addBudgetStyles from "../../../styles/add-budget";

import ClearIcon from "@material-ui/icons/Clear";
import clsx from "clsx";

class VersionHistory extends Component {
  state = {
    dataSource: [
      {
        sample: "history1",
      },
      {
        sample: "history1",
      },
      {
        sample: "history1",
      },
      {
        sample: "history1",
      },
      {
        sample: "history1",
      },
    ],
  };

  handleCloseHistory = () => {
    console.log("button clicked!");
  };

  renderEachHistoryEntry = () => {
    const { classes } = this.props;
    return (
      <Grid item md={12} style={{ marginBottom: "10px" }}>
        <Grid item md={12}>
          <Typography variant="subtitle2" style={{ fontWeight: "600" }}>
            June 12 3:00am
          </Typography>
        </Grid>
        <Grid container alignItems="center">
          <Avatar className={classes.avatar}>MP</Avatar>
          <Typography variant="subtitle2" style={{ opacity: 0.4 }}>
            Michael Pelagio
          </Typography>
        </Grid>
      </Grid>
    );
  };
  render() {
    const { classes } = this.props;
    return (
      <>
        <Grid
          container
          justify="space-between"
          alignItems="center"
          item
          md={12}
        >
          <Typography variant="h1">Version History</Typography>
          <Grid
            container
            justify="center"
            alignItems="center"
            className={classes.historyCloseBtn}
            onClick={this.handleCloseHistory}
          >
            <ClearIcon color="primary" />
          </Grid>
        </Grid>

        <Grid container item className={classes.HistoryContainer}>
          <Grid
            container
            item
            className={clsx(classes.HistoryList, classes.scrollBar)}
          >
            {this.state.dataSource.map((data, index) =>
              this.renderEachHistoryEntry()
            )}
          </Grid>
        </Grid>
      </>
    );
  }
}

export default withStyles(addBudgetStyles.estimatesPanel)(VersionHistory);
