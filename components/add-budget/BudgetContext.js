import React from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { withSnackbar } from "notistack";
import { withRouter } from "next/router";

import { CATEGORIES, STANDARD } from "./constants";
import * as BudgetFormActions from "../../redux/actions/add-budget";

import RateGetter from "../../services/RateGetter";
import TableRateCriteriasSort from "../../services/TableRateCriteriasSort";

const BudgetContext = React.createContext();
class BudgetContextProviderBase extends React.Component {
  static getDerivedStateFromProps(props, prevState) {
    return {
      budget: prevState.budget || props.budget,
      budgetPlan: props.budgetPlan,
    };
  }

  state = {
    budget: null,
    budgetPlan: null,
    visualChartShow: false,
    isDisabledSave: true,
  };

  getTotalPrice = () => {
    let well;
    let leed;
    let totalDesignRate = this.getTotalDesignRate();
    let totalAvgSqft = 0;
    let total = totalDesignRate + totalAvgSqft;

    let checkBudget = _.isEmpty(this.state.budget);
    if (checkBudget != true) {
      well = this.state.budget.well_standard;
      leed = this.state.budget.leed_standard;
      if (well != undefined && leed != undefined) {
        totalAvgSqft = this.getTotalConstructionSqftAverage();
        totalAvgSqft = totalAvgSqft * this.state.budget?.area;
        total = totalDesignRate + totalAvgSqft;
        return total;
      }
    }

    return total;
  };

  getTotalDesignRate = () => {
    const designRate = this.getTotalDesignServiceRate(this.state.budget);
    return designRate * this.state.budget?.area;
  };

  checkTotalPrice = async () => {
    // For enabling save button
    this.setState({ isDisabledSave: false });
    return this.state.isDisabledSave;
  };

  getBaseLineComputation = (typical_value_str) => {
    // debugger;
    const typical_value = parseFloat(typical_value_str);
    const non_typical_rate = (this.state.budget?.non_typical_rate || 0) / 100;
    const uplift_rate = (this.state.budget?.uplift_rate || 0) / 100;
    return typical_value + typical_value * non_typical_rate * uplift_rate;
  };

  getTotalSubCategoryRate = (subCategory, budget) => {
    const rateCriterias = _.filter(
      this.state.budgetPlan?.rate_criterias || [],
      (criteria) =>
        criteria.category === CATEGORIES.DESIGN_SERVICES &&
        criteria["sub-category"] === subCategory &&
        this.isBudgetCriteriaEnabled(budget, criteria)
    );
    const baselineBudgets = _.sumBy(rateCriterias, (criteria) =>
      this.getBaselineRateByCriteria(criteria)
    );
    return baselineBudgets;
  };

  getTotalDesignServiceRate = (budget) => {
    const rateCriterias = _.filter(
      this.state.budgetPlan?.rate_criterias || [],
      (criteria) =>
        (criteria.category === CATEGORIES.DESIGN_SERVICES &&
          this.isBudgetCriteriaEnabled(budget, criteria)) ||
        criteria["sub-category"] === "Preliminaries"
    );

    // rateCriterias.push(
    //   _.find(
    //     this.state.budgetPlan?.rate_criterias,
    //     (obj) => obj["sub-category"] === "Preliminaries"
    //   )
    // );

    const baselineBudgets = _.sumBy(rateCriterias, (criteria) =>
      this.getBaselineRateByCriteria(criteria)
    );
    return baselineBudgets;
  };

  getTotalBaselineBudget = (budget) => {
    const designServiceRate = this.getTotalDesignServiceRate(budget);
    return designServiceRate;
  };

  getFlattenedConstructionServiceRates = (subCategory, standardType) => {
    const rateGroup = _.get(
      this.state.budget.rates,
      `${CATEGORIES.CONSTRUCTION_SERVICES}.${subCategory}`
    );
    const filteredRateGroup = _.pickBy(
      rateGroup,
      (rate) => rate.enabled === standardType
    );
    return _.map(filteredRateGroup, (rateCriteria, code) => ({
      ...rateCriteria,
      code,
    }));
  };

  getTotalConstructionRate = (subCategory, standardType) => {
    const rateCriterias = this.getFlattenedConstructionServiceRates(
      subCategory,
      standardType
    );
    return _.sumBy(rateCriterias, (criteria) =>
      Number(_.get(criteria, `${standardType}.rate`, 0))
    );
  };

  getTotalConstructionValue = (subCategory, standardType) => {
    const constructionRate = this.getTotalConstructionRate(
      subCategory,
      standardType
    );
    return constructionRate * this.state.budget.area;
  };

  getTotalEconomicalValue = () => {
    const construction = this.getTotalConstructionValue(
      CATEGORIES.CONSTRUCTION_SERVICES,
      STANDARD.ECONOMICAL
    );
    const mep = this.getTotalConstructionValue(
      CATEGORIES.MEP_SERVICES,
      STANDARD.ECONOMICAL
    );
    return construction + mep || 0;
  };

  getTotalStandardValue = () => {
    const construction = this.getTotalConstructionValue(
      CATEGORIES.CONSTRUCTION_SERVICES,
      STANDARD.STANDARD
    );
    const mep = this.getTotalConstructionValue(
      CATEGORIES.MEP_SERVICES,
      STANDARD.STANDARD
    );
    return construction + mep || 0;
  };

  checkLeedCriteria = (total, criteria) => {
    let leedValue;
    switch (criteria) {
      case "certified":
        leedValue = total * 0.01;
        break;
      case "silver":
        leedValue = total * 0.025;
        break;
      case "gold":
        leedValue = total * 0.045;
        break;
      case "platinum":
        leedValue = total * 0.1;
        break;
      default:
        leedValue = 0;
    }
    return leedValue;
  };

  checkWellCriteria = (total, criteria) => {
    let wellValue;
    switch (criteria) {
      case "silver":
        wellValue = total * 0.025;
        break;
      case "gold":
        wellValue = total * 0.045;
        break;
      case "platinum":
        wellValue = total * 0.1;
        break;
      default:
        wellValue = 0;
    }
    return wellValue;
  };

  getTotalLeedEconomical = () => {
    const leed = this.state.budget.leed_standard;
    let total;
    let leedValue = 0;

    if (leed != undefined) {
      total = this.getTotalEconomicalValue();
      leedValue = this.checkLeedCriteria(total, leed);
    }

    return leedValue;
  };

  getTotalWellEconomical = () => {
    const well = this.state.budget.well_standard;
    let total;
    let wellValue = 0;

    if (well != undefined) {
      total = this.getTotalEconomicalValue();
      wellValue = this.checkWellCriteria(total, well);
    }

    return wellValue;
  };

  getTotalLeedStandard = () => {
    const leed = this.state.budget.leed_standard;
    let total;
    let leedValue = 0;
    if (leed != undefined) {
      total = this.getTotalStandardValue();
      leedValue = this.checkLeedCriteria(total, leed);
    }
    return leedValue;
  };

  getTotalWellStandard = () => {
    const well = this.state.budget.well_standard;
    let total;
    let wellValue = 0;
    if (well != undefined) {
      total = this.getTotalStandardValue();
      wellValue = this.checkWellCriteria(total, well);
    }
    return wellValue;
  };

  getTotalConstructionServices = () => {
    const well = this.state.budget.well_standard;
    const leed = this.state.budget.leed_standard;
    let totalConstructionService = 0;
    if (well != undefined && leed != undefined) {
      let totalEconomical = this.getTotalEconomicalValue();
      let wellEconomical = this.getTotalWellEconomical();
      let leedEconomical = this.getTotalLeedEconomical();

      let totalStandard = this.getTotalStandardValue();
      let wellStandard = this.getTotalWellStandard();
      let leedStandard = this.getTotalLeedStandard();

      let finalTotalEconomical =
        Math.round(leedEconomical) +
        Math.round(wellEconomical) +
        Math.round(totalEconomical);
      let finalTotalStandard =
        Math.round(leedStandard) +
        Math.round(wellStandard) +
        Math.round(totalStandard);

      totalConstructionService = finalTotalEconomical + finalTotalStandard;
      let addTopUpEcm = totalConstructionService * 0.1;
      totalConstructionService = totalConstructionService + addTopUpEcm;
    }
    return totalConstructionService;
  };

  getTotalConstructionSqftAverage = () => {
    let total = this.getTotalConstructionServices();
    let area = this.state.budget.area;
    area = Number(area);

    return Math.round(total) / Math.round(area);
  };

  checkBudgetForGraphs = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates); // Check for error preventions

    if (checkBudget != true) {
      // If the rates already has rates object this will run
      // The code below rates will get the updated rates from context state
      let rates = _.get(
        this.state.budget.rates,
        `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`
      );

      let mapped = _.map(rates, (obj, key) => {
        // Getting enabled value or rate
        obj.economical["code"] = key;
        obj.standard["code"] = key;
        return _.get(obj, obj.enabled);
      });
      return mapped;
    }
    return false;
  };

  mapCriteriasForGraphs = (object, criteria) => {
    let result;
    let check;
    _.map(object, (obj) => {
      // Getting specific rate to return to graph
      check = _.isUndefined(obj);
      if (check == false && obj.code == criteria) {
        result = Number(obj.rate);
      }
    });
    return result;
  };

  getTotalPartitions = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "partitions"); // Return the rate set to graph
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalWallFinishes = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "wall-finishes"); // Return the rate set to graph
      return result;
    }

    return 0;
  };

  getTotalFlooring = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "flooring"); // Return the rate set to graph
      return result;
    }

    return 0;
  };

  getTotalCeiling = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "ceiling"); // Return the rate set to graph
      return result;
    }

    return 0;
  };

  getTotalLooseFurniture = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "loose-furniture"); // Return the rate set to graph
      return result;
    }

    return 0;
  };

  getTotalMiscellanous = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "miscellanous"); // Return the rate set to graph
      return result;
    }

    return 0;
  };

  getTotalSystemFurniture = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;

    if (checkBudget != false) {
      result = this.mapCriteriasForGraphs(checkBudget, "system-furniture"); // Return the rate set to graph
      return result;
    }

    return 0;
  };

  getTotalPartitionsAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "partitions"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalWallFinishesAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "wall-finishes"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalFlooringAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "flooring"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalCeilingAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "ceiling"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalLooseFurnitureAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "loose-furniture"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalMiscellanousAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "miscellanous"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0; // Default value in graph
  };

  getTotalSystemFurnitureAmount = () => {
    let checkBudget = this.checkBudgetForGraphs();
    let result;
    let area;

    if (checkBudget != false) {
      area = this.state.budget.area;
      result = this.mapCriteriasForGraphs(checkBudget, "system-furniture"); // Return the rate set to graph
      result = result * Number(area);
      return result;
    }

    return 0;
  };

   getTotalPartitions = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates); // Check for error preventions

    if(checkBudget != true){ // If the rates already has rates object this will run
      // The code below rates will get the updated rates from context state
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check; // For checking undefined error prevention
      let mapped = _.map(rates, (obj, key) => { // Getting enabled value or rate
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => { // Getting specific rate to return to graph
        check = _.isUndefined(obj)
        if (check == false && obj.code == "partitions"){
              result = Number(obj.rate);
            }
      });
      
      return result // Return the rate set to graph
    }

    return 0; // Default value in graph
  }

  getTotalWallFinishes = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates);

    if(checkBudget != true){
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check;
      let mapped = _.map(rates, (obj, key) => {
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => {
        check = _.isUndefined(obj)
        if (check == false && obj.code == "wall-finishes"){
              result = Number(obj.rate);
            }
      });
      
      return result
    }

    return 0;
  }

  getTotalFlooring = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates);

    if(checkBudget != true){
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check;
      let mapped = _.map(rates, (obj, key) => {
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => {
        check = _.isUndefined(obj)
        if (check == false && obj.code == "flooring"){
              result = Number(obj.rate);
            }
      });
      
      return result
    }

    return 0;
  }

  getTotalCeiling = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates);

    if(checkBudget != true){
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check;
      let mapped = _.map(rates, (obj, key) => {
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => {
        check = _.isUndefined(obj)
        if (check == false && obj.code == "ceiling"){
              result = Number(obj.rate);
            }
      });
      
      return result
    }

    return 0;
  }

  getTotalLooseFurniture = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates);

    if(checkBudget != true){
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check;
      let mapped = _.map(rates, (obj, key) => {
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => {
        check = _.isUndefined(obj)
        if (check == false && obj.code == "loose-furniture"){
              result = Number(obj.rate);
            }
      });
      
      return result
    }

    return 0;
  }

  getTotalMiscellanous = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates);

    if(checkBudget != true){
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check;
      let mapped = _.map(rates, (obj, key) => {
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => {
        check = _.isUndefined(obj)
        if (check == false && obj.code == "miscellanous"){
              result = Number(obj.rate);
            }
      });
      
      return result
    }

    return 0;
  }

  getTotalSystemFurniture = () => {
    let checkBudget = _.isEmpty(this.state.budget.rates);

    if(checkBudget != true){
      let rates = _.get(this.state.budget.rates, `${CATEGORIES.CONSTRUCTION_SERVICES}.${CATEGORIES.CONSTRUCTION_SERVICES}`);
      
      let check;
      let mapped = _.map(rates, (obj, key) => {
        obj.economical['code'] = key;
        obj.standard['code'] = key;
        return _.get(obj, obj.enabled)
      });

      let result;
       _.map(mapped, (obj) => {
        check = _.isUndefined(obj)
        if (check == false && obj.code == "system-furniture"){
              result = Number(obj.rate);
            }
      });
      
      return result
    }

    return 0;
  }

  handleRequired = (dataRow, bool) => {
    const newBudget = { ...this.state.budget };

    const first_index = _.findIndex(
      newBudget.design_services_table,
      (data) => data.category === dataRow.category
    );

    const second_index = _.findIndex(
      newBudget.design_services_table[first_index].data_row,
      (data) => _.isEqual(data, dataRow)
    );

    newBudget.design_services_table[first_index].data_row[
      second_index
    ].is_required = bool;

    console.log("newDesignServicesTable: ", newBudget);

    this.setState({ budget: newBudget });
  };

  setRatesGivenByArea = (rates) => {
    let newBudget = _.set(this.state.budget, `rate_by_area`, rates);
    this.setState({ budget: newBudget });
  };

  isBudgetCriteriaEnabled(budget, criteria) {
    return (
      _.get(
        budget,
        `rates.${criteria.category}.${criteria["sub-category"]}.${criteria.id}.enabled`
      ) === true
    );
  }

  async componentDidMount() {
    if (this.props.budget?.budget_plan) {
      await this.props.getBudgetPlanById(this.props.budget.budget_plan);
    }
  }

  setBudget = (budget) => {
    this.setState({ budget });
  };

  setTypicalFloorRates = (typ_rate) => {
    let newBudget = _.set(this.state.budget, `typical_rate`, typ_rate);
    this.setState({ budget: newBudget });
    // console.log("this is typical rates:", newBudget);
  };

  setNonTypicalFloorRates = (non_typ_rate) => {
    let newBudget = _.set(this.state.budget, `non_typical_rate`, non_typ_rate);
    this.setState({ budget: newBudget });
    // console.log("this is non_typical_rate rates:", newBudget);
  };

  setUplift = (value) => {
    let newBudget = _.set(this.state.budget, `uplift_rate`, Number(value));
    this.setState({ budget: newBudget });
  };

  setCurrency = (location) => {
    let newBudget = _.set(this.state.budget, `currency`, location.currency);
    this.setState({ budget: newBudget });
  };

  getDesignServicesRateGroup = () => {
    if (this.state.budgetPlan) {
      const filteredRateCriterias = _.filter(
        this.state.budgetPlan.rate_criterias,
        (criteria) => criteria.category === CATEGORIES.DESIGN_SERVICES
      );
      const groups = _.groupBy(
        filteredRateCriterias,
        (criteria) => criteria["sub-category"]
      );

      groups["Preliminaries"] = [
        _.find(
          this.state.budgetPlan.rate_criterias,
          (obj) => obj["sub-category"] === "Preliminaries"
        ),
      ];

      let ordered = new TableRateCriteriasSort().setOrderDesignServices(groups);
      return ordered || [];
    }
  };

  getConstructionServiceRates = () => {
    if (this.state.budgetPlan) {
      const filteredRateCriterias = _.filter(
        this.state.budgetPlan.rate_criterias,
        (criteria) =>
          criteria.category === CATEGORIES.CONSTRUCTION_SERVICES &&
          criteria["sub-category"] === CATEGORIES.CONSTRUCTION_SERVICES
      );

      const filteredCriteriasWithValue = _.map(
        filteredRateCriterias,
        (rateCriteria) => {
          const defaultValue = this.getTypicalRateByCriteria(rateCriteria);
          return { ...rateCriteria, defaultValue };
        }
      );

      this.decorateConstructionServiceDefaultValues(filteredCriteriasWithValue);
      let ordered = new TableRateCriteriasSort().setOrderConstructionServices(
        filteredCriteriasWithValue
      );

      return ordered || [];
    }
  };

  getMEPServicesRates = () => {
    if (this.state.budgetPlan) {
      const filteredRateCriterias = _.filter(
        this.state.budgetPlan.rate_criterias,
        (criteria) =>
          criteria.category === CATEGORIES.CONSTRUCTION_SERVICES &&
          criteria["sub-category"] === CATEGORIES.MEP_SERVICES
      );
      const filteredCriteriasWithValue = _.map(
        filteredRateCriterias,
        (rateCriteria) => {
          const defaultValue = this.getTypicalRateByCriteria(rateCriteria);
          return { ...rateCriteria, defaultValue };
        }
      );
      this.decorateConstructionServiceDefaultValues(filteredCriteriasWithValue);
      let ordered = new TableRateCriteriasSort().setOrderMEPServices(
        filteredCriteriasWithValue
      );
      return ordered || [];
    }
  };

  decorateConstructionServiceDefaultValues = (criterias) => {
    _.forEach(criterias, (criteria) => {
      if (
        !_.has(
          this.state.budget,
          `rates.${criteria.category}.${criteria["sub-category"]}.${criteria.code}.standard.rate`
        )
      ) {
        _.set(
          this.state.budget,
          `rates.${criteria.category}.${criteria["sub-category"]}.${criteria.code}.standard.rate`,
          criteria.defaultValue
        );
      }
      if (
        !_.has(
          this.state.budget,
          `rates.${criteria.category}.${criteria["sub-category"]}.${criteria.code}.economical.rate`
        )
      ) {
        _.set(
          this.state.budget,
          `rates.${criteria.category}.${criteria["sub-category"]}.${criteria.code}.economical.rate`,
          criteria.defaultValue
        );
      }
    });
    this.setBudget(this.state.budget);
  };

  getCurrency = () => {
    return this.state.budgetPlan?.currency
      ? this.state.budgetPlan.currency
      : "----";
  };

  getBaselineRateByCriteria = (rateCriteria) => {
    // debugger;
    if (rateCriteria["sub-category"] !== "Preliminaries") {
      const typicalRate = this.getTypicalRateByCriteria(rateCriteria);
      return Number(this.getBaseLineComputation(typicalRate));
    }
    if (rateCriteria["sub-category"] === "Preliminaries") {
      if (_.isEmpty(this.state.budget)) {
        //this fix the issue of "undefined rate_by_area"
        return;
      }
      return Number(this.state.budget.rate_by_area?.preliminaries || 0);
    }
  };

  setNewPreliminariesValue = (newValue) => {
    const newBudget = { ...this.state.budget };
    this.state.budget.rate_by_area["preliminaries"] = newValue;

    this.setState({ budget: newBudget });
  };

  getTypicalRateByCriteria = (rateCriteria) => {
    // debugger;
    if (rateCriteria) {
      const rates = new RateGetter().getRatesByArea(
        this.state.budgetPlan,
        Number(this.state.budget?.area)
      );
      return _.get(rates, rateCriteria.id, 0);
    }
  };

  saveBudget = async () => {
    const price = _.round(this.getTotalPrice(), 2);
    const { uid, displayName, email, photoURL } = this.props.agentDetails;
    const budget = {
      ...this.state.budget,
      price,
      agent: { uid, displayName, email, photoURL },
    };

    console.log("the user details:", budget);

    try {
      if (_.has(budget, "id")) {
        await this.props.updateBudget(budget);
        this.props.enqueueSnackbar("Budget is successfully updated", {
          variant: "success",
        });
      } else {
        await this.props.createBudget(budget);
        this.props.enqueueSnackbar("Budget is successfully created", {
          variant: "success",
        });
      }
      this.props.router.push("/budget");
    } catch (error) {
      console.log(error);
      this.props.enqueueSnackbar(
        "Oops! Seems like there's an error. Please try again!",
        { variant: "error" }
      );
    }
  };

  showVisualCharts = () => {
    this.setState({ visualChartShow: true });
  };

  render() {
    return (
      <BudgetContext.Provider
        value={{
          ...this.state,
          ...this,
        }}
      >
        {this.props.children}
      </BudgetContext.Provider>
    );
  }
}

export default BudgetContext;

const mapStateToProps = (state) => ({
  budgetPlan: state.addBudget.budgetPlan,
  agentDetails: state.firebase.auth,
});

const mapDispatchToProps = (dispatch) => ({
  createBudget: (budget) => {
    return dispatch(BudgetFormActions.createBudget(budget));
  },
  updateBudget: (budget) => {
    return dispatch(BudgetFormActions.updateBudget(budget));
  },
  getBudgetPlanById: (budgetPlanId) => {
    return dispatch(BudgetFormActions.getBudgetPlanById(budgetPlanId));
  },
});

export const BudgetContextProvider = connect(
  mapStateToProps,
  mapDispatchToProps
)(withSnackbar(withRouter(BudgetContextProviderBase)));
