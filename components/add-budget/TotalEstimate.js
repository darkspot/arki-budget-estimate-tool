import React, { Component } from "react";
import { Grid, withStyles, Typography, Modal, Paper } from "@material-ui/core";

import NumberFormat from "react-number-format";
import { connect } from "react-redux";

import SmallBarCharts from "../charts/SmallBarCharts";
import SmallPieChart from "../charts/SmallPieChart";
import BigBarChart from "../charts/BigBarCharts";
import BigPieChart from "../charts/BigPieChart";

import VersionHistory from "./estimate-panel-components/VersionHistory";

import * as addBudgetActions from "../../redux/actions/add-budget";
import * as addBudgetStyles from "../../styles/add-budget";
import BudgetContext from "./BudgetContext";

import _ from "lodash";

class TotalEstimatePanel extends Component {
  state = {
    budgetBreakDownGraphSampleData: [
      {
        name: "Total Amount",
        value: 6500,
      },
      {
        name: "Total Construction & MEP - Economical ",
        value: 3000,
      },
      {
        name: "Total Construction & MEP - Standard",
        value: 2500,
      },
    ],
  };

  static contextType = BudgetContext;

  handleClose = () => {
    this.setState({ modal: false });
  };
  handleOpen = () => {
    this.setState({ modal: true });
  };

  handleOpacityOnChange = () => {
    // console.log("color change: ", this.context.state.budget);
    if (
      _.every(
        ["area", "currency"],
        _.partial(_.has, this.context.state.budget)
      ) ||
      this.context.state.budget?.currency
    ) {
      return false;
    }
    return true;
  };

  get breakdownData() {
    return [
      {
        name: "Total Amount",
        value: this.context.getTotalPrice(),
      },
      {
        name: "Total Construction & MEP - Economical ",
        value: this.context.getTotalEconomicalValue(),
      },
      {
        name: "Total Construction & MEP - Standard",
        value: this.context.getTotalStandardValue(),
      },
    ];
  }

  get breakdownDesignData() {
    const subCategories = [
      "Design Services",
      "Building Assessment",
      "Strategy",
      "Programming",
      "Code and Environment",
      "Project Leadership",
      "Preliminaries",
      "Construction Services",
      "MEP Services",
    ];
    return _.map(subCategories, (cat) => ({
      name: cat,
      value: this.context.getTotalSubCategoryRate(cat, this.context.budget),
    }));
  }

  get graphConstruction() {
    return [
      {
        name: "Partitions",
        value: this.context.getTotalPartitions(),
      },
      {
        name: "Flooring",
        value: this.context.getTotalFlooring(),
      },
      {
        name: "Ceilings",
        value: this.context.getTotalCeiling(),
      },
      {
        name: "wall-finishes",
        value: this.context.getTotalWallFinishes(),
      },
      {
        name: "Loose Furniture",
        value: this.context.getTotalLooseFurniture(),
      },
      {
        name: "Miscellaneous",
        value: this.context.getTotalMiscellanous(),
      },
      {
        name: "System Furniture",
        value: this.context.getTotalSystemFurniture(),
      },
    ];
  }

  get graphConstructionTotals() {
    return [
      {
        name: "Partitions",
        value: this.context.getTotalPartitionsAmount(),
      },
      {
        name: "Flooring",
        value: this.context.getTotalFlooringAmount(),
      },
      {
        name: "Ceilings",
        value: this.context.getTotalCeilingAmount(),
      },
      {
        name: "wall-finishes",
        value: this.context.getTotalWallFinishesAmount(),
      },
      {
        name: "Loose Furniture",
        value: this.context.getTotalLooseFurnitureAmount(),
      },
      {
        name: "Miscellaneous",
        value: this.context.getTotalMiscellanousAmount(),
      },
      {
        name: "System Furniture",
        value: this.context.getTotalSystemFurnitureAmount(),
      },
    ];
  }

  get graphEconomicalAndStandard() {
    return [
      {
        name: "Economical",
        value: this.context.getTotalEconomicalValue(),
      },
      {
        name: "Standard",
        value: this.context.getTotalStandardValue(),
      },
    ];
  }

  render() {
    const { classes } = this.props;
    return (
      <>
        {false && (
          <Grid container item md={12} sm={12}>
            <VersionHistory />
          </Grid>
        )}

        <Grid
          container
          item
          sm={12}
          justify="space-between"
          alignItems="center"
          style={{ height: "80px" }}
        >
          <Typography variant="h1">Total estimates</Typography>
        </Grid>

        <Grid
          container
          // justify="center"
          alignItems="center"
          item
          sm={12}
          style={{
            padding: "20px",
            // marginTop: "40px",
            marginBottom: "40px",
            borderRadius: "20px",
            backgroundColor: "#F8F9FE ",
            height: "125px",
            width: "100%",
          }}
        >
          <span
            className={classes.total}
            // style={}
          >
            {this.context.getCurrency()}&nbsp;
            <NumberFormat
              value={this.context.getTotalPrice() || 0}
              displayType={"text"}
              decimalScale={2}
              thousandSeparator={true}
            />
          </span>
        </Grid>

        <Grid
          className={classes.scrollBar}
          item
          sm={12}
          style={{ overflow: "auto", maxHeight: "calc(100vh - 403px)" }}
        >
          <Grid
            item
            style={{
              padding: "20px",
              // marginTop: "40px",
              borderRadius: "20px",
              backgroundColor: "#F8F9FE ",
              // height: "125px",
              width: "100%",
            }}
          >
            <Grid
              item
              style={{ opacity: this.handleOpacityOnChange() ? "0.3" : "1" }}
            >
              <Grid
                item
                style={{
                  fontSize: "24px",
                  // color: "#D0D1D2",
                  fontWeight: "bold",
                }}
              >
                <NumberFormat
                  value={this.context.budget?.area || 0}
                  displayType={"text"}
                  thousandSeparator={true}
                />
                &nbsp;sqft
              </Grid>
              <Grid
                item
                style={{
                  fontSize: "18px",
                  // color: "#D0D1D2"
                }}
              >
                Total Area
              </Grid>
            </Grid>
            <Grid
              item
              style={{
                marginTop: "10px",
                opacity: this.handleOpacityOnChange() ? "0.3" : "1",
              }}
            >
              <Grid
                item
                style={{
                  fontSize: "24px",
                  // color: "#D0D1D2",
                  fontWeight: "bold",
                }}
              >
                {this.context.getCurrency()}&nbsp;
                <NumberFormat
                  value={
                    this.context.getTotalBaselineBudget(this.context.budget) ||
                    0
                  }
                  displayType={"text"}
                  decimalScale={2}
                  thousandSeparator={true}
                />
              </Grid>
              <Grid
                item
                style={{
                  fontSize: "18px",

                  // color: "#D0D1D2"
                }}
              >
                Baseline Budget per (sqft)
              </Grid>

              {this.props.currentStep >= 2 && (
                <>
                  <Grid container spacing={1}>
                    <Grid
                      item
                      xs={7}
                      style={{
                        fontSize: "24px",
                        marginTop: 25,
                        marginBottom: 0,
                        // color: "#D0D1D2",
                        fontWeight: "bold",
                      }}
                    >
                      {this.context.getCurrency()}&nbsp;
                      <NumberFormat
                        value={this.context.getTotalEconomicalValue() || 0}
                        displayType={"text"}
                        decimalScale={2}
                        thousandSeparator={true}
                      />
                      <p
                        style={{
                          fontSize: "18px",
                          marginTop: 5,
                          fontWeight: "normal",
                        }}
                      >
                        Total Economical
                      </p>
                    </Grid>
                    <Grid
                      item
                      xs={5}
                      style={{
                        marginTop: 12,
                      }}
                    >
                      LEED AMOUNT <br></br>
                      <div style={{ fontWeight: "bold", fontSize: 16 }}>
                        {this.context.getCurrency()}&nbsp;
                        <NumberFormat
                          value={this.context.getTotalLeedEconomical() || 0}
                          displayType={"text"}
                          decimalScale={2}
                          thousandSeparator={true}
                        />
                      </div>
                      WELL AMOUNT <br></br>
                      <div style={{ fontWeight: "bold", fontSize: 16 }}>
                        {this.context.getCurrency()}&nbsp;
                        <NumberFormat
                          value={this.context.getTotalWellEconomical() || 0}
                          displayType={"text"}
                          decimalScale={2}
                          thousandSeparator={true}
                        />
                      </div>
                    </Grid>
                  </Grid>

                  <Grid container spacing={1}>
                    <Grid
                      item
                      xs={7}
                      style={{
                        fontSize: "24px",
                        marginTop: 25,
                        marginBottom: 0,
                        // color: "#D0D1D2",
                        fontWeight: "bold",
                      }}
                    >
                      {this.context.getCurrency()}&nbsp;
                      <NumberFormat
                        value={this.context.getTotalStandardValue() || 0}
                        displayType={"text"}
                        decimalScale={2}
                        thousandSeparator={true}
                      />
                      <p
                        style={{
                          fontSize: "18px",
                          marginTop: 5,
                          fontWeight: "normal",
                        }}
                      >
                        Total Standard
                      </p>
                    </Grid>
                    <Grid
                      item
                      xs={5}
                      style={{
                        marginTop: 12,
                      }}
                    >
                      LEED AMOUNT <br></br>
                      <div style={{ fontWeight: "bold", fontSize: 16 }}>
                        {this.context.getCurrency()}&nbsp;
                        <NumberFormat
                          value={this.context.getTotalLeedStandard() || 0}
                          displayType={"text"}
                          decimalScale={2}
                          thousandSeparator={true}
                        />
                      </div>
                      WELL AMOUNT <br></br>
                      <div style={{ fontWeight: "bold", fontSize: 16 }}>
                        {this.context.getCurrency()}&nbsp;
                        <NumberFormat
                          value={this.context.getTotalWellStandard() || 0}
                          displayType={"text"}
                          decimalScale={2}
                          thousandSeparator={true}
                        />
                      </div>
                    </Grid>
                  </Grid>
                </>
              )}
            </Grid>
          </Grid>

          {this.props.currentStep >= 2 && (
            <Grid
              container
              spacing={1}
              style={{
                padding: "35px",
                marginTop: "15px",
                borderRadius: "20px",
                backgroundColor: "#E3DEF7 ",
                // height: "125px",
                width: "100%",
              }}
            >
              <Grid
                item
                xs={12}
                style={{
                  fontSize: "24px",
                  marginBottom: 0,
                  color: "#5F49B2",
                  fontWeight: "bold",
                }}
              >
                {this.context.getCurrency()}&nbsp;
                <NumberFormat
                  value={this.context.getTotalConstructionServices() || 0}
                  displayType={"text"}
                  decimalScale={2}
                  thousandSeparator={true}
                />
                <p
                  style={{
                    fontSize: "18px",
                    marginTop: 5,
                    color: "black",
                    fontWeight: "normal",
                  }}
                >
                  Grand Total Construction and MEP Cost
                </p>
              </Grid>
              <Grid
                item
                xs={12}
                style={{
                  fontSize: "24px",
                  color: "#5F49B2",
                  marginTop: 5,
                  marginBottom: 0,
                  fontWeight: "bold",
                }}
              >
                {this.context.getCurrency()}&nbsp;
                <NumberFormat
                  value={this.context.getTotalConstructionSqftAverage() || 0}
                  displayType={"text"}
                  decimalScale={2}
                  thousandSeparator={true}
                />
                <p
                  style={{
                    fontSize: "18px",
                    marginTop: 5,
                    color: "black",
                    fontWeight: "normal",
                  }}
                >
                  Total Average Per Sqft
                </p>
              </Grid>
            </Grid>
          )}

          {this.props.currentStep >= 2 && <Grid container spacing={1}></Grid>}

          {this.props.currentStep >= 2 && <Grid container spacing={1}></Grid>}
          {this.context.visualChartShow == true && (
            <>
              <Grid
                container
                item
                justify="space-between"
                alignItems="center"
                style={{ height: "80px", marginTop: "20px" }}
              >
                <Typography variant="h1">Visuals</Typography>

                <Typography
                  variant="subtitle2"
                  color="secondary"
                  style={{ marginRight: "10px" }}
                >
                  Click to view
                </Typography>
              </Grid>

              <Grid
                item
                className={classes.smallGrapContainer}
                onClick={() => this.props.handleGraphModal(1)}
              >
                <SmallBarCharts
                  title="Budget Breakdown - Total Value"
                  type={1}
                  data={this.breakdownData} //insert the data from redux in the future
                />
              </Grid>
              <Grid
                item
                className={classes.smallGrapContainer}
                onClick={() => this.props.handleGraphModal(1.1)}
              >
                <SmallBarCharts
                  title="Budget Breakdown - Sqft"
                  type={1}
                  data={this.breakdownDesignData} //insert the data from redux in the future
                />
              </Grid>
              {this.props.currentStep >= 2 && (
                <>
                  <Grid
                    item
                    className={classes.smallGrapContainer}
                    onClick={() => this.props.handleGraphModal(2)}
                  >
                    <SmallBarCharts
                      title="Construction Service - SqFt"
                      type={2}
                      data={this.graphConstruction} //insert the data from redux  in the future
                    />
                  </Grid>
                  <Grid
                    item
                    className={classes.smallGrapContainer}
                    onClick={() => this.props.handleGraphModal(3)}
                  >
                    <SmallBarCharts
                      title="Construction Service - Total Amount"
                      type={3}
                      data={this.graphConstructionTotals} //insert the data from redux in the future
                    />
                  </Grid>

                  <Grid
                    item
                    className={classes.smallGrapContainer}
                    onClick={() => this.props.handleGraphModal(4)}
                  >
                    <SmallPieChart data={this.graphEconomicalAndStandard} />
                  </Grid>
                </>
              )}
            </>
          )}
        </Grid>

        <Modal
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
          disableAutoFocus
          open={this.props.isGraphModalOpen} //this.state.modal
          onClose={() => this.props.handleCloseModal()}
        >
          <Grid item className={classes.bigGraphContainer}>
            {/* bel */}
            {this.props.typeOfGraph === 1 && (
              <BigBarChart
                data={this.breakdownData}
                type={1}
                title="Budget Breakdown - Total Value"
              />
            )}
            {this.props.typeOfGraph === 1.1 && (
              <BigBarChart
                data={this.breakdownDesignData}
                type={1}
                title="Budget Breakdown - Sq Ft"
              />
            )}
            {this.props.typeOfGraph === 2 && (
              <BigBarChart
                data={this.graphConstruction}
                type={2}
                title="Construction Service - SqFt"
              />
            )}
            {this.props.typeOfGraph === 3 && (
              <BigBarChart
                title="Construction Service - Total Amount"
                data={this.graphConstructionTotals} //@Daryll double check if the data for the graph is accurate
                type={3}
              />
            )}

            {this.props.typeOfGraph === 4 && (
              <BigPieChart data={this.graphEconomicalAndStandard} />
            )}

            {/* <BigBarChart data={this.state.constructionSqFtSampleData} /> */}
          </Grid>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isGraphModalOpen: state.addBudget.isGraphModalOpen,
  typeOfGraph: state.addBudget.typeOfGraph,
  currentStep: state.addBudget.activeStep,
});
const mapDispatchToProps = (dispatch) => ({
  handleGraphModal: (typeOfGraph) => {
    return dispatch(addBudgetActions.handleGraphModal(typeOfGraph));
  },

  handleCloseModal: () => {
    return dispatch(addBudgetActions.handleCloseGraphModal());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(addBudgetStyles.estimatesPanel)(TotalEstimatePanel));
