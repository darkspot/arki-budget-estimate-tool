import React, { Component } from "react";

//MUI
import {
  withStyles,
  Grid,
  Typography,
  Button,
  Step,
  Stepper,
  StepLabel,
  Container,
} from "@material-ui/core";
//styles
import * as addBudgetStyles from "../../styles/add-budget";
//redux
import { connect } from "react-redux";
import * as addBudgetActions from "../../redux/actions/add-budget";

//components
import ProjectDetails from "./add-budget-components/ProjectDetails";
import DesignServiceForm from "./add-budget-components/DesignServiceForm";
import ConstructionServices from "./add-budget-components/ConstructionServices";
import MEPServices from "./add-budget-components/MEPServices";
import BudgetContext from "./BudgetContext";
import _ from "lodash";

class BudgetFormPanel extends Component {
  static contextType = BudgetContext;

  handleNextStep = () => {
    const stepList = this.props.steps;
    const currentActiveStep = this.props.activeStep;
    const currentStepObj = this.props.currentSteps;
    this.props.setNextStep(stepList, currentActiveStep, currentStepObj);
  };

  handlePrevStep = () => {
    const stepList = this.props.steps;
    const currentActiveStep = this.props.activeStep;
    const currentStepObj = this.props.currentSteps;
    this.props.setPrevStep(stepList, currentActiveStep, currentStepObj);
  };

  handleFinishStep = () => {
    alert("last step");
  };

  handleClickOnStepper = (stepObj, stepIndex) => {
    if (stepObj.isCompleted) {
      this.props.clickedSteps(stepObj, stepIndex);
    }
  };

  handleDisabled = () => {
    if (
      _.every(
        ["area", "client_name", "budget_plan", "address", "project_title"],
        _.partial(_.has, this.context.state.budget)
      )
    ) {
      return false;
    }
    return true; //--> turn this to true for enabling validation
  };
  render() {
    const { classes } = this.props;

    // console.log("this is content in app budget panel: ", this.context.state);
    return (
      <Grid container>
        <Grid item lg={12} md={12} sm={12} xs={12}>
          <Grid container item className="acb-component-header">
            <Grid item lg={6} md={6} sm={4} xs={4} style={{ padding: "20px" }}>
              <Typography
                variant="h1"
                // className="acb-component-header-title"
              >
                Add Capex Budget
              </Typography>
            </Grid>
            <Grid item lg={6} md={6} sm={8} xs={8}>
              <div className="acb-component-header-flex-end">
                <Button
                  disabled={this.context.isDisabledSave}
                  onClick={() => this.context.saveBudget()}
                  variant="contained"
                  color="primary"
                  stye={{ width: "130px" }}
                  className={classes.headerBtn}
                  // className="acb-component-header-button-save"
                  // disableElevation
                >
                  SAVE
                </Button>
                <Button
                  disabled
                  variant="contained"
                  stye={{ width: "255px" }}
                  className={classes.headerBtn}
                  // className="acb-component-header-button-download"
                  disableElevation
                >
                  DOWNLOAD AS PDF
                </Button>
              </div>
            </Grid>
          </Grid>

          <Grid
            container
            alignItems="center"
            lg={12}
            md={12}
            sm={12}
            xs={12}
            className={classes.subtleBorderLine}
            style={{ height: "120px" }}
          >
            <Grid item lg={12} md={12} sm={12} xs={10}>
              <Typography variant="h2" align="center">
                {this.props.currentSteps.label}
              </Typography>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={8}>
              <Stepper
                activeStep={this.props.activeStep}
                connector={<Grid item md={2}></Grid>}
              >
                {this.props.steps.map((steps, index) => (
                  <Step
                    style={{ cursor: "pointer" }}
                    key={steps.label}
                    completed={steps.isCompleted}
                    active={steps.isActive}
                    onClick={() => this.handleClickOnStepper(steps, index)}
                  >
                    <StepLabel>{steps.label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
            </Grid>
          </Grid>

          <Grid
            container
            lg={12}
            md={12}
            sm={12}
            xs={12}
            justify="center"
            style={{
              height: "100%",
              maxHeight: "calc(100vh - 280px)",
              // overflowY: "auto",
              // overflowX: "hidden",
            }}
          >
            <Grid item lg={12} md={12} sm={12} xs={12}>
              {this.props.activeStep === 0 && <ProjectDetails />}
              {this.props.activeStep === 1 && <DesignServiceForm />}
              {this.props.activeStep === 2 && <ConstructionServices />}
              {this.props.activeStep === 3 && <MEPServices />}
            </Grid>
            <Grid
              item
              lg={12}
              md={12}
              sm={12}
              xs={12}
              style={{ marginTop: "15px" }}
            >
              {!this.props.isLastStep && (
                <Grid container justify="flex-end">
                  {!this.props.isFirstStep && (
                    <Button
                      disabled={this.props.isFirstStep}
                      onClick={() => this.handlePrevStep()}
                      variant="contained"
                      style={{
                        marginRight: "20px",
                        backgroundColor: "#F0F2F5",
                        marginBottom: "75px",
                      }}
                      disableElevation
                    >
                      Back
                    </Button>
                  )}

                  <Button
                    onClick={(event) => {
                      this.handleNextStep();
                      this.context.showVisualCharts();
                      this.context.checkTotalPrice();
                    }}
                    variant="contained"
                    style={{
                      marginTop: "0px",
                      marginRight: "50px",
                      marginBottom: "75px",
                      backgroundColor: "#D1CAED",
                      width: "200px",
                      color: "rgba(66, 47, 138, 0.87)",
                      opacity: this.handleDisabled() ? "0.3" : "1",
                    }}
                    disabled={this.handleDisabled()}
                    disableElevation
                  >
                    Next
                  </Button>
                </Grid>
              )}

              {this.props.isLastStep && (
                <Grid container justify="flex-end">
                  <Button
                    onClick={() => this.handlePrevStep()}
                    variant="contained"
                    style={{ marginRight: "20px", backgroundColor: "#F0F2F5" }}
                    disableElevation
                  >
                    Back
                  </Button>
                  <Button
                    onClick={() => this.context.saveBudget()}
                    variant="contained"
                    style={{ marginRight: "20px", backgroundColor: "#F0F2F5" }}
                    disableElevation
                  >
                    Finish
                  </Button>
                </Grid>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({
  steps: state.addBudget.steps,
  activeStep: state.addBudget.activeStep,
  currentSteps: state.addBudget.currentSteps,
  isLastStep: state.addBudget.isLastStep,
  isFirstStep: state.addBudget.isFirstStep,
});
const mapDispatchToProps = (dispatch) => ({
  setNextStep: (stepList, currentActiveStep, currentStepObj) => {
    return dispatch(
      addBudgetActions.setNextStep(stepList, currentActiveStep, currentStepObj)
    );
  },
  clickedSteps: (stepObj, stepIndex) => {
    return dispatch(addBudgetActions.clickedSteps(stepObj, stepIndex));
  },
  setPrevStep: (stepList, currentActiveStep, currentStepObj) => {
    return dispatch(
      addBudgetActions.setPrevStep(stepList, currentActiveStep, currentStepObj)
    );
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(addBudgetStyles.addBudgetPanel)(BudgetFormPanel));
