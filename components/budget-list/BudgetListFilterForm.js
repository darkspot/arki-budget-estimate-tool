import React from "react";

// redux & actions
import { connect } from "react-redux";
import * as BudgetListActions from "../../redux/actions/budget-list";
import * as addBudgetActions from "../../redux/actions/add-budget";

// Material
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import Input from '@material-ui/core/Input';

// Icons

// Final Form

// Lodash for Debounce
import * as _ from 'lodash';


// Next JS
import { withRouter } from "next/router";
import { Field, Form } from "react-final-form";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class BudgetListFilterForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      areas: [
        10000,
        25000,
        50000,
        75000,
        100000,
        150000,
        200000,
        300000,
        400000,
        500000,
        600000,
        700000,
        800000,
        900000,
        1000000,
      ],
      setSelectedAreas: [],
      prices: [
        10000,
        25000,
        50000,
        75000,
        100000,
        150000,
        200000,
        300000,
        400000,
        500000,
        600000,
        700000,
        800000,
        900000,
        1000000,
      ],
      setSelectedPrices: [],
    };
  }

  handleChangeArea = async (event) => {
    await this.setState({ setSelectedAreas: event.target.value });
    console.log(this.state.setSelectedAreas);
  };

  handleChangePrice = async (event) => {
    await this.setState({ setSelectedPrices: event.target.value });
    console.log(this.state.setSelectedPrices);
  };

  componentDidMount() {
  }

  handleSubmit = async (values) => {
    await this.props.filterArea(values);
  }

  render() {
    const { country_list, city_list } = this.props;
    const { areas, setSelectedAreas, prices, setSelectedPrices } = this.state;
    return (
      <Form
        onSubmit={this.handleSubmit}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              <Grid item xs={3}>
                <p className="dl-component-description">Country</p>
                <Field name="countryFilter">
                  {props => (
                    <div>
                      <Select className="dl-component-select" value={props.input.value}
                        onChange={props.input.onChange} variant="outlined" displayEmpty
                        renderValue={(value) => value || "Select Country"}>
                        {country_list.map((country, index) => (
                          <MenuItem key={index} value={country.name}>
                            {country.name}
                          </MenuItem>
                        ))}
                      </Select>
                    </div>
                  )}
                </Field>
              </Grid>
              <Grid item xs={3}>
                <p className="dl-component-description">City</p>
                <Field name="cityFilter">
                  {props => (
                    <div>
                      <Select className="dl-component-select" value={props.input.value}
                        onChange={props.input.onChange} variant="outlined" displayEmpty
                        renderValue={(value) => value || "Select City"}>
                        {city_list.map((city, index) => (
                          <MenuItem key={index} value={city.location}>
                            {city.location}
                          </MenuItem>
                        ))}
                      </Select>
                    </div>
                  )}
                </Field>
              </Grid>
              <Grid item xs={3}>
                <p className="dl-component-description">Select Area</p>
                <Field name="areaFilter">
                  {props => (
                    <div>
                      <Select
                        className="dl-component-select"
                        multiple
                        variant="outlined"
                        value={setSelectedAreas}
                        onChange={this.handleChangeArea}
                        variant="outlined" displayEmpty
                        renderValue={(selected) => selected.join(', ')}
                        MenuProps={MenuProps}
                      >
                        {areas.map((area) => (
                          <MenuItem key={area} value={area}>
                            <Checkbox checked={setSelectedAreas.indexOf(area) > -1} />
                            <ListItemText primary={area} />
                          </MenuItem>
                        ))}
                      </Select>
                    </div>
                  )}
                </Field>
              </Grid>
              <Grid item xs={3}>
                <p className="dl-component-description">Select Price</p>
                <Field name="priceFilter">
                  {props => (
                    <div>
                      <Select
                        className="dl-component-select"
                        multiple value={setSelectedPrices}
                        onChange={this.handleChangePrice}
                        variant="outlined"
                        displayEmpty
                        renderValue={(selected) => selected.join(', ')}
                        MenuProps={MenuProps}
                      >
                        {prices.map((price) => (
                          <MenuItem key={price} value={price}>
                            <Checkbox checked={setSelectedPrices.indexOf(price) > -1} />
                            <ListItemText primary={price} />
                          </MenuItem>
                        ))}
                      </Select>
                    </div>
                  )}
                </Field>
              </Grid>
              {/* <Grid item xs={2} className="dl-component-capex-button-grid-style">
                <Button variant="contained" type="submit" disabled={submitting} className="dl-component-search-button" style={{ backgroundColor: "#F0F2F5", color: "#65676B", marginTop: 52 }} disableElevation>
                  SEARCH
                </Button>
              </Grid> */}
            </Grid>
          </form>
        )} />
    )
  }
}

const mapStateToProps = (state) => ({
  country_list: state.budgetList.budget_list_get_countries,
  data_rows: state.budgetList.budget_list_get_rows,
  city_list: state.addBudget.add_budget_get_locations,
});
const mapDispatchToProps = (dispatch) => ({
  getRows: () => {
    return dispatch(BudgetListActions.getRows());
  },
  getCountries: () => {
    return dispatch(BudgetListActions.getCountries());
  },
  filterArea: (filterData) => {
    return dispatch(BudgetListActions.filterArea(filterData));
  },
  searchFilter: (filterData) => {
    return dispatch(BudgetListActions.searchFilter(filterData));
  },
  pushCountryData: () => {
    return dispatch(BudgetListActions.pushCountryData());
  },
  pushBudgetListData: () => {
    return dispatch(BudgetListActions.pushBudgetListData());
  },
  getLocations: () => {
    return dispatch(addBudgetActions.getLocations());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(BudgetListFilterForm)
);