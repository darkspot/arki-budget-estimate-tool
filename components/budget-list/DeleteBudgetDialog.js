import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Typography } from '@material-ui/core';
import axios from 'axios';
import { withSnackbar } from 'notistack';

class DeleteBudgetDialog extends React.Component {

  handleClose = (succcess) => {
    const { onClose } = this.props;
    onClose && onClose(succcess);
  }

  handleSuccessDeletion = async () => {
    try {
      const { onClose } = this.props;
      await axios.delete(`/api/budgets/${this.props.budget.id}`);
      this.props.enqueueSnackbar("Budget is successfully removed", {
        variant: "success",
      });
      onClose && onClose(true);
    } catch (error) {
      this.props.enqueueSnackbar("There was an error while removing this budget. Please try again later", {
        variant: "error",
      });
    }
  }


  render() {
    const { open } = this.props;
    return (
      <div>
        <Dialog open={open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Delete Budget</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <Typography variant="h6">Are you sure?</Typography>
              <Typography variant="body2">If you delete the budget. you can't recover it</Typography>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button variant="contained" onClick={this.handleSuccessDeletion} color="primary">
              Yes, Delete it
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withSnackbar(
  DeleteBudgetDialog
);