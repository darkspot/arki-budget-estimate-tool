import React, { Component } from "react";
import NumberFormat from "react-number-format";

// redux & actions
import { connect } from "react-redux";
import * as BudgetListActions from "../../redux/actions/budget-list";
import * as addBudgetActions from "../../redux/actions/add-budget";

// Material
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Search from '@material-ui/icons/Search';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

// Icons
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import DeleteOutlineOutlinedIcon from '@material-ui/icons/DeleteOutlineOutlined';

// Final Form

// Lodash for Debounce
import * as _ from 'lodash';


// Next JS
import { withRouter } from "next/router";
import BudgetListFilterForm from "./BudgetListFilterForm";
import DeleteBudgetDialog from "./DeleteBudgetDialog";

class BudgetList extends Component {

  constructor(props) {
    super(props)
    this.state = {
      filterValue: "",
    };
    this.handleSearchFilter = this.handleSearchFilter.bind(this);
  }

  handleSubmit = async (values) => {
    await this.props.filterArea(values);
  }

  handleSearchFilter = async (event) => {
    await this.setState({ filterValue: event.target.value });

    console.log(this.state.filterValue);

    if (event.target.value == "" || null) {
      let debounce = _.debounce(() => this.props.searchFilter(null), 1500);
      debounce();
    } else {
      let debounce = _.debounce(() => this.props.searchFilter(this.state.filterValue), 2500);
      debounce();
    }
  }

  handleEditButton = async (value) => {
    this.props.router.push(`/budget/${value.id}`);
    console.log(value);
  }

  handleDeleteButton = async (value) => {
    this.setState({ open: true, budget: value });
  }

  handleCloseDialog = async () => {
    this.setState({ open: false, budget: null });
    await this.props.getRows();
  }

  componentDidMount = async () => {
    await this.props.getCountries();
    await this.props.getLocations();
    await this.props.getRows();
    // await this.props.pushBudgetListData(); // For uploading constants data of table once.
    // await this.props.pushCountryData(); // For uploading constants data of country once.
  }

  render() {
    const { data_rows } = this.props;
    return (
      <div>
        <div> {/* Header Part */}
          <Grid container spacing={3} lg={12} md={12} sm={12} xs={12}>
            <Grid item lg={3} md={3} sm={3} xs={3}>
              <p className="dl-component-title">Budgets</p>
            </Grid>
            <Grid item lg={9} md={9} sm={9} xs={9} className="dl-component-capex-button-grid-style">
              <Button variant="contained" onClick={() => this.props.router.push('/add-budget')} className="dl-component-capex-button" style={{ backgroundColor: "#422F8A", color: "#ffffff", marginTop: 12.5 }} disableElevation>
                ADD CAPEX BUDGET
              </Button>
            </Grid>
          </Grid>
        </div>

        <div> {/* Second Grid Search and Filter*/}
          <Grid container spacing={2} lg={12} md={12} sm={12} xs={12}>
            <Grid item lg={3} md={3} sm={3} xs={3}>
              <p className="dl-component-description">What are you looking for?</p>
              <OutlinedInput className="dl-component-search-bar" color="primary" onChange={this.handleSearchFilter}
                startAdornment={<InputAdornment position="start">
                  <Search />
                </InputAdornment>} placeholder="Search ..." />
            </Grid>
            <Grid item lg={9} md={9} sm={9} xs={9} className="dl-component-capex-button-grid-style">
              <BudgetListFilterForm />
            </Grid>
          </Grid>
        </div>

        <div className="dl-component-table-container">   {/* Data Table Section*/}
          {/* <DataGrid
        rows={data_rows}
        columns={Columns}
        pageSize={5}
      /> */}
        <Grid container lg={12} md={12} sm={12} xs={12}>
          <TableContainer style={{ maxHeight: 350 }} component={Paper}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Client Name</TableCell>
                  <TableCell>Project Name</TableCell>
                  <TableCell>Location</TableCell>
                  <TableCell>Area</TableCell>
                  <TableCell>Price</TableCell>
                  <TableCell>Last Modified</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data_rows.map((row, rowIndex) => (
                  <TableRow key={rowIndex}>
                    <TableCell>{row.client_name}</TableCell>
                    <TableCell>{row.project_title}</TableCell>
                    <TableCell>{row.location}</TableCell>
                    <TableCell>
                      <NumberFormat
                        value={row.area || 0}
                        displayType={'text'}
                        thousandSeparator={true} /> sqft</TableCell>
                    <TableCell>
                      <NumberFormat
                        value={row?.price || 0}
                        displayType={"text"}
                        decimalScale={2}
                        thousandSeparator={true}
                      /> {row.currency}</TableCell>
                    <TableCell>{row.lastModified}</TableCell>
                    <TableCell>
                      <Button
                        variant="contained"
                        style={{ marginRight: 10 }}
                        className="dl-component-manage-buttons"
                        disableElevation
                        onClick={() => this.handleEditButton(row)}
                        startIcon={<EditOutlinedIcon className="dl-component-manage-buttons-icon" />}
                      ><p className="dl-component-manage-buttons-text">Edit</p></Button>
                      <Button
                        variant="contained"
                        className="dl-component-manage-buttons"
                        disableElevation
                        onClick={() => this.handleDeleteButton(row)}
                        startIcon={<DeleteOutlineOutlinedIcon className="dl-component-manage-buttons-icon" />}
                      ><p className="dl-component-manage-buttons-text">Delete</p></Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <DeleteBudgetDialog
              open={this.state.open}
              budget={this.state.budget}
              onClose={this.handleCloseDialog}
            />
          </TableContainer>
          </Grid>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  // sideBarValue: state.mainLayout.sideBarValue,
  // tabValue: state.mainLayout.tabValue,
  country_list: state.budgetList.budget_list_get_countries,
  data_rows: state.budgetList.budget_list_get_rows,
  city_list: state.addBudget.add_budget_get_locations,
});
const mapDispatchToProps = (dispatch) => ({
  // handleSideBar: (value) => {
  //   return dispatch(MainLayOutActions.handleSideBar(value));
  // },
  getRows: () => {
    return dispatch(BudgetListActions.getRows());
  },
  getCountries: () => {
    return dispatch(BudgetListActions.getCountries());
  },
  filterArea: (filterData) => {
    return dispatch(BudgetListActions.filterArea(filterData));
  },
  searchFilter: (filterData) => {
    return dispatch(BudgetListActions.searchFilter(filterData));
  },
  pushCountryData: () => {
    return dispatch(BudgetListActions.pushCountryData());
  },
  pushBudgetListData: () => {
    return dispatch(BudgetListActions.pushBudgetListData());
  },
  getLocations: () => {
    return dispatch(addBudgetActions.getLocations());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(BudgetList));
