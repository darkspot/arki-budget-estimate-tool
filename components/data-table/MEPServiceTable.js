import React, { Component } from "react";
import {
  Grid,
  Typography,
  Radio,
  withStyles,
  TextField,
} from "@material-ui/core";

import _ from "lodash";

import * as addBudgetStyles from "../../styles/add-budget";
import { Field, Form, FormSpy } from "react-final-form";
import BudgetContext from "../add-budget/BudgetContext";
import InlineTextField from "./InlineTextField";
import NumberFormat from "react-number-format";

class MEPServiceTable extends Component {
  state = { mepServices: null };

  static contextType = BudgetContext;

  componentWillMount() {
    const mepServices = this.context.getMEPServicesRates();
    this.setState({ mepServices })
    console.log("rateGroup", mepServices);
  }

  handleSubmit = (formValues) => {
    this.context.setBudget(formValues)
  }

  handleChangeBudget = (formProps) => {
    this.context.setBudget(formProps.values);
  }

  removeRateCriteria = (formProps, path) => {
    console.log(formProps);
    formProps.form.change(`${path}.enabled`, false);
  }

  renderHeader() {
    const { classes } = this.props;
    return (
      <Grid container item className={classes.subtleBorderLine} md={12} lg={12} xs={12} sm={12}>
        <Grid
          container
          justify="center"
          className={classes.constructServicesHeader}
          item
          md={4} lg={4} xs={4} sm={4}
        >
          Services
      </Grid>
        <Grid
          container
          justify="center"
          item
          md={4} lg={4} xs={4} sm={4}
          className={classes.constructServicesHeader}
          style={{
            backgroundColor: "#F8F9FE",
          }}
        >
          Economical Range
      </Grid>
        <Grid
          container
          justify="center"
          item
          md={4} lg={4} xs={4} sm={4}
          className={classes.constructServicesHeader}
          style={{
            backgroundColor: "#D1CAED",
          }}
        >
          Standard Range
      </Grid>
      </Grid>
    )
  }

  renderForm(formProps) {
    const { classes } = this.props;
    return (
      <form onSubmit={formProps.handleSubmit}>
        <FormSpy
          subscription={{ values: true }}
          onChange={this.handleChangeBudget} />
        <Grid item style={{ marginTop: "10px" }} md={12} lg={12} xs={12} sm={12}>
          {this.renderHeader()}
          <Grid
            item
            className={classes.scrollBar}
            style={{
              marginTop: "10px",
              border: "1px solid #ECE9F1",
              maxHeight: "calc(100vh - 500px)",
              height: "calc(100vh - 500px)",
              overflow: "auto",
            }}
            md={12} lg={12} xs={12} sm={12}
          >
            {_.map(this.state.mepServices, (rateCriteria) => (
              this.renderRateCriteria(rateCriteria, formProps)
            ))}
          </Grid>
        </Grid>
      </form>
    );
  }

  renderSelectionTile = (formValues, rateCriteria, selectionType) => {
    const rateCriteriaPath = `rates.${rateCriteria.category}.${rateCriteria['sub-category']}.${rateCriteria.code}`;
    const tileNotePath = `${rateCriteriaPath}.${selectionType}.notes`;
    const tileRatePath = `${rateCriteriaPath}.${selectionType}.rate`;
    const tileEnabledPath = `${rateCriteriaPath}.enabled`;
    const currency = _.get(formValues, `currency`, 0);
    const isTileEnabled = _.get(formValues, tileEnabledPath, false) === selectionType;
    const { classes } = this.props;
    return (
      <Grid
        container
        alignContent="space-between"
        item 
        md={4} lg={4} xs={4} sm={4}
        style={{ width: "100%" }}
        className={isTileEnabled ? classes.boxIsEnabled : classes.boxIsDisabled}
      >
        <Grid item md={12} lg={12} xs={12} sm={12} className={classes.inLineEditContainer}>
          <Field
            name={tileNotePath}
            render={(fieldProps) => (
              <InlineTextField classes={this.props.classes} {...fieldProps.input} />
            )}
          />
        </Grid>
        <Grid container item alignItems="center" md={12} lg={12} xs={12} sm={12}>
          <Grid item md={2} lg={2} xs={2} sm={2}>
            <Field
              name={tileEnabledPath}
              render={(fieldProps) => (
                <Radio
                  value={selectionType}
                  checked={fieldProps.input.value === selectionType}
                  onChange={(event) => fieldProps.input.onChange(event)} />
              )} />
          </Grid>
          <Grid item md={4} lg={4} xs={4} sm={4}>
            <Typography><b>Select</b></Typography>
            {isTileEnabled && <Typography>YES</Typography>}
          </Grid>
          <Grid item md={6} lg={6} xs={6} sm={6}>
            <Field
              name={tileRatePath}
              render={(fieldProps) => (
                <NumberFormat
                  {...fieldProps}
                  defaultValue={10}
                  value={fieldProps.input.value}
                  customInput={TextField}
                  type="text"
                  label="Rate"
                  disabled={!isTileEnabled}
                  suffix={currency}
                  variant="outlined"
                  thousandSeparator
                  onValueChange={({ value: v }) =>
                    fieldProps.input.onChange({ target: { name, value: v } })
                  }
                  inputProps={{
                    style: { textAlign: "right" },
                  }}
                />
              )} />
          </Grid>
        </Grid>
      </Grid>
    )
  }


  renderRateCriteria(rateCriteria, formProps) {
    const { classes } = this.props;
    return <Grid
      container
      item
      md={12} lg={12} xs={12} sm={12}
      className={classes.rowBottomBorder}
    >
      <Grid item  md={4} lg={4} xs={4} sm={4}>
        <Grid container direction="column"
          justifyContent="space-between"
          alignItems="flex-start"
          style={{ fontWeight: "bolder", padding: "10px" }}
        >
          <Grid item md={12} lg={12} xs={12} sm={12}>
            {rateCriteria.label}
          </Grid>
          <Grid item md={12} lg={12} xs={12} sm={12} onClick={() => this.removeRateCriteria(formProps, rateCriteriaPath)}>
            Remove this in estimate
          </Grid>
        </Grid>
      </Grid>
      {this.renderSelectionTile(formProps.values, rateCriteria, "economical")}
      {this.renderSelectionTile(formProps.values, rateCriteria, "standard")}
    </Grid>;
  }

  render() {
    return <Form
      onSubmit={this.handleSubmit}
      initialValues={this.context.budget}
      render={(formProps) => this.renderForm(formProps)}
    />
  }
}

export default withStyles(addBudgetStyles.addBudgetPanel)(MEPServiceTable);
