/* eslint-disable @typescript-eslint/no-use-before-define */
// import * as React from "react";
import React, { Component } from "react";
import * as _ from "lodash";
import { DataGrid } from "@material-ui/data-grid";
import { makeStyles, withStyles, Grid } from "@material-ui/core";
import { connect } from "react-redux";
import * as BudgetPlanActions from "../../redux/actions/budget-plans";

const useStyles = (theme) => ({
  header: {
    "& .MuiDataGrid-columnsContainer": {
      backgroundColor: "#fbfbfb",
    },
  },
  headerText: {
    "& .MuiDataGrid-columnHeaderTitle": {
      color: theme.palette.secondary.main,
      fontWeight: "bold",
    },
  },
});

class DataTableGrid extends Component {
  state = {};

  getAdditionalColumns = (areaRanges) => {
    // const classes = useStyles();
    const { classes } = this.props;

    const areaRangeColumns = _.map(areaRanges || [], (range) => {
      return {
        field: range.code,
        headerName: `${range.name} sqft`,
        headerClassName: classes.headerText,
        headerAlign: "center",
        type: "number",
        align: "center",
        editable: this.props.onEdit,
        width: 200,
      };
    });
    return areaRangeColumns;
  };

  generateRowsByCriterias = (criterias, budgetPlan) => {
    return _.map(criterias, (criteria) => {
      const budgetPlanRates = _.get(budgetPlan, `rates.${criteria.code}`, {});
      return {
        id: criteria.code,
        criteria: `${criteria.category} - ${criteria.name}`,
        ...budgetPlanRates,
      };
    });
  };

  handlCellChange = async ({ props, ...otherProperty }) => {
    // console.log("handleCell change: " + otherProperty);
    this.props.setTableCellChange({ ...otherProperty, newValue: props.value });
  };

  render() {
    const { classes, criterias, areaRanges, budgetPlan } = this.props;

    const columns = [
      {
        field: "criteria",
        headerName: "PRE-DESIGN CAPEX BUDGET",
        headerClassName: classes.headerText,
        // headerAlign: "center",
        editable: this.props.onEdit, //onEdit
        width: 600,
      },
      ...this.getAdditionalColumns(areaRanges),
    ];

    const rows = this.generateRowsByCriterias(criterias, budgetPlan);

    return (
        <Grid container spacing={1} style={{height: 400, width: "100%", marginRight: 10, marginLeft: 10 }} lg={12} md={12} sm={12} xs={12}>
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <DataGrid
              className={classes.header}
              rows={rows}
              columns={columns}
              // autoHeight
              // pageSize={7}
              hideFooter
              // isCellEditable={(params) => handleCellEdit(params)}
              onEditCellChangeCommitted={(params) => this.handlCellChange(params)}
            />
          </Grid>
        </Grid>
    );
  }
}
const mapStateToProps = (state) => ({
  currentPlan: state.budgetPlans.currentPlan,
  onEdit: state.budgetPlans.onEdit,
});

const mapDispatchToProps = (dispatch) => ({
  setTableCellChange: async (data) => {
    return await dispatch(BudgetPlanActions.setTableCellChange(data));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(DataTableGrid));
