import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';

class InlineTextField extends React.Component {
  state = {
    isEditable: false
  }

  setEditable(value) {
    this.setState({ isEditable: value });
  }

  render() {
    const { classes } = this.props;
    return (
      <>
        {!this.state.isEditable && !this.props.value?.length && (
          <Grid container className={classes.noValuePlaceholder} alignItems="center"
            onClick={() => this.setEditable(true)}
          >
            <Grid item md={12}>
              <Typography variant="body1">Please write any additional notes here</Typography>
            </Grid>
          </Grid>
        )}
        {!this.state.isEditable && this.props.value?.length > 0 && (
          <Grid container className={classes.inlineTextField}
            onClick={() => this.setEditable(true)}
          >
            <Grid item md={12}>
              <Typography variant="body1">{this.props.value}</Typography>
            </Grid>
          </Grid>
        )}
        {this.state.isEditable && (
          <TextField
            className={classes.inlineTextField}
            value={this.props.value}
            rows={3}
            rowsMax={3}
            multiline
            fullWidth
            variant="outlined"
            onChange={this.props.onChange}
            onBlur={() => this.setEditable(false)}
          />
        )}
      </>
    );
  }
}

export default InlineTextField;