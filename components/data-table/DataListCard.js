import React, { Component } from "react";
import { withSnackbar } from "notistack";
//MUI
import {
  Grid,
  withStyles,
  Select,
  Card,
  MenuItem,
  Typography,
  Button,
  Icon,
  CircularProgress,
} from "@material-ui/core";
import ClearIcon from "@material-ui/icons/Clear";
import SaveIcon from "@material-ui/icons/Save";
//components
import DataTableGrid from "./DataTableGrid";
import BudgetPlanTab from "../budget-plans/BudgetPlanTab";
//styles
import mainStyles from "./../../styles/main-layout";
//redux
import { connect } from "react-redux";
import * as DataTableActions from "../../redux/actions/data-table";
import * as BudgetPlanActions from "../../redux/actions/budget-plans";

class MainLayout extends Component {
  state = {};

  async componentDidMount() {
    const {
      getBudgetPlans,
      getAreaRanges,
      getCriterias,
      enqueueSnackbar,
    } = this.props;
    try {
      await getBudgetPlans();

      // await getDataTables();
      await getAreaRanges(this.props.currentPlan.area_ranges);
      await getCriterias(this.props.currentPlan.rate_criterias);
    } catch (error) {
      enqueueSnackbar(error.message, { variant: "error" });
    }
  }

  handleChange = (event) => {
    if (event.target.name === "show") {
      console.log("change");
      this.setState({ show: event.target.value });
    } else if (event.target.name === "pricing_rate") {
      // console.log("change");
      this.setState({ pricing_rate: event.target.value });
    }
  };

  onEditTable = () => {
    this.props.onEditBudgetPlan(true);
  };

  onCancelEdit = () => {
    this.props.onEditBudgetPlan(false);
    this.props.clearDataCollections(); //clear the data
  };

  onSaveData = async () => {
    this.props.onEditBudgetPlan(false);

    try {
      await this.props.onSaveDataToFirebase(
        this.props.currentPlan.rates,
        this.props.dataOnEditChange,
        this.props.currentPlan.id,
        this.props.currentPlan.location
      );
      this.props.enqueueSnackbar("Data Saved", { variant: "success" });
    } catch (e) {
      if (e.name == "NetworkError") {
        this.props.enqueueSnackbar("Error: " + e.message, { variant: "error" });
        setTimeout(() => window.location.reload(), 3000); //handles network error and refresh the browser after 3 s
      }

      this.props.enqueueSnackbar("Error: " + e.message, { variant: "error" });
    }
  };

  render() {
    const { areaRanges, budgetPlans, currentPlan, criterias, isPageLoading } =
      this.props;
    return (
      <Grid container spacing={3}>
        <Grid item md={12} sm={12} xs={12}>
          <Card>
            {budgetPlans && <BudgetPlanTab budgetPlans={budgetPlans} />}

            <Grid container style={{ margin: "20px 0px" }} md={12} sm={12} xs={12} item>
              <Grid container md={3} item alignItems="center">
                {/* <Typography style={{ marginLeft: "20px" }} variant="h6">
                  Show
                </Typography>
                <Select
                  style={{ marginLeft: "20px" }}
                  name="show"
                  value={this.state.show || ""}
                  variant="outlined"
                  onChange={(event) => this.handleChange(event)}
                  displayEmpty
                  renderValue={(value) => value || "All Pre-Designs Budget"}
                  //place holdershit
                >
                  <MenuItem value={10}>Ten</MenuItem>
                  <MenuItem value={20}>Twenty</MenuItem>
                  <MenuItem value={30}>Thirty</MenuItem>
                </Select> */}
              </Grid>
              <Grid container md={6} sm={6} xs={6} item alignItems="center">
                <Typography style={{ marginLeft: "20px" }} variant="h6">
                  Standard Sqft Pricing Rates for Typical Floors (sqft)
                </Typography>
                <Select
                  style={{ marginLeft: "20px" }}
                  name="pricing_rate"
                  value={this.state.pricing_rate || ""}
                  variant="outlined"
                  onChange={(event) => this.handleChange(event)}
                  displayEmpty
                  renderValue={(value) => value || "Pricing Rate"}
                >
                  <MenuItem value={"0-10k"}>0-10k</MenuItem>
                  <MenuItem value={"10k-25k"}>10k-25k</MenuItem>
                  <MenuItem value={"25k-50k"}>25k-50k</MenuItem>
                  <MenuItem value={"50k-75k"}>50k-75k</MenuItem>
                  <MenuItem value={"75k-100k"}>75k-100k</MenuItem>
                  <MenuItem value={"100k-150k"}>100k-150k</MenuItem>
                  <MenuItem value={"150k-200k"}>150k-200k</MenuItem>
                  <MenuItem value={"200k-300k"}>200k-300k</MenuItem>
                  <MenuItem value={"300k-400k"}>300k-400k</MenuItem>
                  <MenuItem value={"400k-500k"}>400k-500k</MenuItem>
                  <MenuItem value={"500k-600k"}>500k-600k</MenuItem>
                  <MenuItem value={"600k-700k"}>600k-700k</MenuItem>
                  <MenuItem value={"700k-800k"}>700k-800k</MenuItem>
                  <MenuItem value={"800k-900k"}>800k-900k</MenuItem>
                  <MenuItem value={"900k-1M"}>900k-1M</MenuItem>
                  <MenuItem value={"1M+"}>1M+</MenuItem>
                </Select>
              </Grid>

              <Grid container md={3} sm={3} xs={3} item justify="flex-end">
                {!this.props.onEdit && (
                  <Button
                    onClick={() => this.onEditTable()}
                    variant="contained"
                    startIcon={<Icon className="far fa-edit" />}
                    style={{ marginRight: "20px", backgroundColor: "#F0F2F5" }}
                    disableElevation
                  >
                    Edit
                  </Button>
                )}

                {this.props.onEdit && (
                  <>
                    <Button
                      onClick={() => this.onCancelEdit()}
                      variant="contained"
                      startIcon={<ClearIcon />}
                      style={{
                        marginRight: "20px",
                        backgroundColor: "red",
                      }}
                      disableElevation
                    >
                      Cancel
                    </Button>
                    <Button
                      onClick={() => this.onSaveData()}
                      variant="contained"
                      startIcon={<SaveIcon />}
                      style={{
                        marginRight: "20px",
                        backgroundColor: "green",
                      }}
                      disableElevation
                    >
                      Save
                    </Button>
                  </>
                )}
              </Grid>
            </Grid>
          </Card>
        </Grid>
        {this.props.isPageLoading &&
          <Grid container
            alignItems="center"
            style={{ height: 'calc(100vh - 600px)' }}
            justify="center">
            <Grid item>
              <CircularProgress color="secondary" />
            </Grid>
          </Grid>
        }
        {areaRanges && !isPageLoading && (
          <Grid container  justify="center" lg={12} sm={12} md={12} xs={12}>
              <DataTableGrid
                criterias={criterias}
                budgetPlan={currentPlan}
                areaRanges={areaRanges}
              />
          </Grid>
        )}
      </Grid>
    );
  }
}

const mapStateToProps = (state) => ({
  list: state.dataTable.list,
  areaRanges: state.dataTable.areaRanges,
  criterias: state.dataTable.criterias,
  budgetPlans: state.budgetPlans.list,
  currentPlan: state.budgetPlans.currentPlan,
  isPageLoading: state.budgetPlans.isPageLoading,
  // location: state.budgetPlans.currentPlan.location,
  // currentRates: state.budgetPlans.currentPlan.rates,
  onEdit: state.budgetPlans.onEdit,
  dataOnEditChange: state.budgetPlans.dataOnEditChange,
});

const mapDispatchToProps = (dispatch) => ({
  getDataTables: async () => {
    return await dispatch(DataTableActions.getDataTables());
  },
  getAreaRanges: (area_ranges) => {
    return dispatch(DataTableActions.getAreaRanges(area_ranges));
  },
  getCriterias: (criterias) => {
    return dispatch(DataTableActions.getCriterias(criterias));
  },
  getBudgetPlans: async () => {
    return await dispatch(BudgetPlanActions.getBudgetPlans());
  },
  onEditBudgetPlan: (boolean) => {
    return dispatch(BudgetPlanActions.onEditBudgetPlan(boolean));
  },
  clearDataCollections: () => {
    return dispatch(BudgetPlanActions.clearDataCollections());
  },
  onSaveDataToFirebase: async (
    currentRates,
    newRates,
    budgetPlanId,
    location
  ) => {
    return dispatch(
      BudgetPlanActions.onSaveDataToFirebase(
        currentRates,
        newRates,
        budgetPlanId,
        location
      )
    );
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSnackbar(withStyles(mainStyles)(MainLayout)));
