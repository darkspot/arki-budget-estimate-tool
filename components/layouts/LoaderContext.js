import { Backdrop, CircularProgress } from "@material-ui/core";
import React, { Component } from "react";

export const LoaderContext = React.createContext();

class LoaderContextProvider extends Component {
  state = {
    loading: false,
  }

  setLoading = (loading) => {
    this.setState({ loading });
  }

  render() {
    return (
      <LoaderContext.Provider value={{
        ...this.state,
        setLoading: this.setLoading,
      }}>
        <Backdrop open={this.state.loading} >
          <CircularProgress color="inherit" />
        </Backdrop>
        {this.props.children}
      </LoaderContext.Provider>
    )
  }
}

export default LoaderContextProvider;