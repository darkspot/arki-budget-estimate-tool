import React, { Component } from "react";
//MUI
import { withStyles } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/core/styles";

//themes
import Main from "../theme/Main";
//components
import TopNav from "./TopNav";
import SideBar from "./SideBar";
//styles
import mainStyles from "./../../styles/main-layout";

class MainLayout extends Component {
  state = {};
  render() {
    const { classes, children, noPadding } = this.props;
    // console.log(classes);
    return (
      <>
        <ThemeProvider theme={Main}>
          <div style={{ display: "flex", position: "fixed" }}>
            <SideBar />

            <div className={classes.mainContainer}>
              <TopNav />
              <main
                style={noPadding && { padding: "0px" }}
                className={classes.main}
              >
                {children}
              </main>
            </div>
          </div>
        </ThemeProvider>
      </>
    );
  }
}

export default withStyles(mainStyles)(MainLayout);
