import React, { Component } from "react";
//MUI
import { withStyles } from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import IconButton from "@material-ui/core/IconButton";
import Drawer from "@material-ui/core/Drawer";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

//styles
import mainStyles from "./../../styles/main-layout";
//redux & actions
import { connect } from "react-redux";
import * as MainLayOutActions from "../../redux/actions/main-layout";

class SideBar extends Component {

  handleClick = () => {
    console.log("side nav bar click");
  };
  render() {
    const { classes } = this.props;
    return (
      <>
        <div
          className={
            !this.props.sideBarValue ? classes.sideNav : classes.sideNavClose
          }
        >
          <div style={{ position: "fixed" }}>
            <IconButton
              disableRipple
              onClick={() => this.props.handleSideBar(true)}
            >
              <MenuIcon className={classes.hamburgerIcon} />
            </IconButton>
          </div>
        </div>
        <Drawer
          variant="persistent"
          open={this.props.sideBarValue}
          classes={{
            paper: classes.drawerPaper,
          }}
          className={
            this.props.sideBarValue ? classes.drawer : classes.drawerClose
          }
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={() => this.props.handleSideBar(false)}>
              {/* hatdog */}
              <ChevronLeft style={{ fontSize: "35px" }} />
            </IconButton>
          </div>

          <Divider />

          <List>
            <ListItem button disableRipple>
              sample 1
            </ListItem>
            <ListItem button disableRipple>
              sample 2
            </ListItem>
            <ListItem button disableRipple>
              sample 3
            </ListItem>
          </List>
        </Drawer>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  sideBarValue: state.mainLayout.sideBarValue,

});
const mapDispatchToProps = (dispatch) => ({
  handleSideBar: (value) => {
    return dispatch(MainLayOutActions.handleSideBar(value));
  },

});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(mainStyles)(SideBar));
