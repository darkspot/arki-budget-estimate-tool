import React, { Component } from "react";
//next
import { withRouter } from "next/router";
//mui component
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Toolbar from "@material-ui/core/Toolbar";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import { Menu, MenuItem, withStyles } from "@material-ui/core";
import Avatar from "react-avatar";
//redux
import { connect } from "react-redux";
import * as MainLayOutActions from "../../redux/actions/main-layout";
import * as AuthActions from "../../redux/actions/auth";
//components
//style
import mainStyles from "./../../styles/main-layout";

class TopNav extends Component {
  state = {
    profileMenu: false,
    menuComponent: null,
  };

  handleRedirectPage = (value) => {
    this.props.router.push(value);
    this.props.handleTabChange(value);
  };

  componentDidMount() {
    const { handleTabChange } = this.props;

    if (this.props.router.pathname === "/add-budget") {
      handleTabChange("/budget");
    } else {
      handleTabChange(this.props.router.pathname);
    }
  }

  handleOpenProfileMenu = (event) => {
    this.setState({ profileMenu: event.currentTarget });
  };

  handleCloseProfileMenu = () => {
    this.setState({ profileMenu: null });
  };

  handleLogin = () => {
    this.props.handleLogin();
  };

  handleLogout = () => {
    this.props.handleLogout();
    this.handleCloseProfileMenu();
  };

  renderProfileMenu() {
    return (
      <Menu
        id="menu-appbar"
        anchorEl={this.state.menuComponent}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={this.state.profileMenu}
        onClose={this.handleCloseProfileMenu}
      >
        <MenuItem onClick={this.handleCloseProfileMenu}>Profile</MenuItem>
        {this.props.auth.isEmpty && (
          <MenuItem onClick={this.handleLogin}>Login</MenuItem>
        )}
        {!this.props.auth.isEmpty && (
          <MenuItem onClick={this.handleLogout}>Logout</MenuItem>
        )}
      </Menu>
    );
  }

  render() {
    const { tabValue, classes } = this.props;
    return (
      <AppBar position="sticky" className="topnav-shadow">
        <Toolbar>
          <Grid container item md={12} sm={12}>
            <Grid item md={4} sm={4}>
              <Tabs
                value={tabValue}
                onChange={(event, value) => this.handleRedirectPage(value)}
              >
                <Tab disableRipple value="/" label="Rate Matrix" wrapped />

                <Tab disableRipple value="/budget" label="Budget" wrapped />
              </Tabs>
            </Grid>

            <Grid container item md={4} sm={4} alignItems="center"></Grid>

            <Grid item md={3} sm={3}></Grid>
            <Grid container item md={1} sm={1} justify="flex-end">
              <Avatar
                aria-controls="menu-appbar"
                name={this.props.auth.photoUrl || this.props.auth.displayName}
                size="40"
                round={true}
                onClick={this.handleOpenProfileMenu}
              />
              {this.renderProfileMenu()}
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.firebase.auth,
  authError: state.auth.authErrork,
  tabValue: state.mainLayout.tabValue,
});
const mapDispatchToProps = (dispatch) => ({
  handleTabChange: (value) => {
    return dispatch(MainLayOutActions.handleTopBarChange(value));
  },
  handleLogin: (value) => {
    return dispatch(AuthActions.handleLogin());
  },
  handleLogout: (value) => {
    return dispatch(AuthActions.handleLogout());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(mainStyles)(withRouter(TopNav)));
