import { createMuiTheme } from "@material-ui/core/styles";
import createBreakpoints from "@material-ui/core/styles/createBreakpoints";

const breakpoints = createBreakpoints({});

const theme = createMuiTheme({
  typography: {
    fontFamily: ["Poppins", "Roboto"],
  },
  palette: {
    primary: {
      main: "#FFF",
      secondary: "#F8F9FE",
    },
    secondary: {
      main: "rgba(66, 47, 138, 0.87)", ///violet
      // secondary: "red",
    },
  },
  overrides: {
    MuiTypography: {
      colorInherit: {
        opacity: 0.6,
      },
      h1: {
        [breakpoints.up("md")]: {
          fontSize: "24px",
          fontWeight: "bolder",
        },
        [breakpoints.down("md")]: {
          fontSize: "18px",
          fontWeight: "bolder",
        },
      },

      h2: {
        [breakpoints.up("md")]: {
          fontSize: "20px",
          fontWeight: 500,
        },
        [breakpoints.down("md")]: {
          fontSize: "14px",
          fontWeight: "bolder",
        },
      },

      h6: {
        fontSize: "16px",
        fontWeight: "bold",
      },
      // colorPrimary:"#000 !important"
    },
    MuiOutlinedInput: {
      root: {
        "&$focused fieldset": {
          borderColor: "rgba(66, 47, 138, 0.87) !important",
          borderWidth: "1px !important",
        },
      },

      input: {
        padding: "10.5px 14px",
      },
      // focused: {
      //   borderColor: "black",
      //   borderWidth: "1px",
      // },
      // notchedOutline: {
      //   borderColor: "black",
      //   borderWidth: "1px",
      // },
    },
    // MuiLink: {
    //   color: "#000",
    // },
    // MuiBreadcrumbs:{
    //   li:{
    //     color:"black"
    //   }
    // },
    MuiInput: {
      underline: {
        "&:after": {
          //under line field
          borderBottom: "2px solid #0062cc",
        },
      },
    },
    MuiInputLabel: {
      formControl: {
        top: "-15px",
      },
    },
    MuiToolbar: {
      regular: {
        [breakpoints.up("md")]: {
          minHeight: "48px",
          marginTop: "10px",
        },
      },
    },

    MuiTab: {
      root: {
        fontWeight: 700,
        textTransform: "Capitalize",
      },
    },

    MuiStepLabel: {
      root: {
        [breakpoints.up("md")]: {
          width: "200px",
        },
        [breakpoints.down("md")]: {
          width: "150px",
        },
      },
      label: {
        [breakpoints.up("md")]: {
          fontSize: "15px",
        },
        [breakpoints.down("md")]: {
          fontSize: "12px",
        },
        // fontSize: "12px",
      },

      active: {
        color: "rgba(66, 47, 138, 0.87) !important",
      },
      // iconContainer: "rgba(66, 47, 138, 0.87)",
    },

    MuiStepIcon: {
      root: {
        "&$completed": {
          color: "rgba(66, 47, 138, 0.87)",
        },
        "&$active": {
          color: "rgba(66, 47, 138, 0.87)",
        },
      },
      text: {
        fill: "white",
      },
      // active: {
      //   "& circle": {
      //     color: "rgba(66, 47, 138, 0.87)",
      //   },
      // },

      // text: {
      //   color: "white",
      // },
    },
    //this will the over rides of css of MUI componnet
  },
});

export default theme;
