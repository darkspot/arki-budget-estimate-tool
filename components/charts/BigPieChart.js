import React, { Component } from "react";

import { Grid, withStyles, Typography } from "@material-ui/core";

import { PieChart, Pie, Cell, Legend, ResponsiveContainer } from "recharts";

const alternateColor = ["#D1CAED", "rgba(66, 47, 138, 0.87)"];

class BigPieChart extends Component {
  state = {};

  renderCustomLegends = (api) => {
    return (
      <Grid
        container
        item
        justify="flex-end"
        md={12}
        style={{ marginBottom: 5 }}
      >
        <Grid
          container
          item
          alignItems="center"
          md={4}
          style={{ fontSize: "14px" }}
        >
          <Grid
            item
            style={{
              height: 15,
              width: 15,
              backgroundColor: "#D1CAED",
              borderRadius: "10px",
              marginRight: "5px",
            }}
          />
          <span>Economical</span>
        </Grid>

        <Grid
          container
          item
          alignItems="center"
          md={4}
          style={{ fontSize: "14px" }}
        >
          <Grid
            item
            style={{
              height: 15,
              width: 15,
              backgroundColor: "rgba(66, 47, 138, 0.87)",
              borderRadius: "10px",
              marginRight: "5px",
            }}
          />
          <span>Standard</span>
        </Grid>
      </Grid>
    );
  };

  renderValue = () => {
    const { data } = this.props;
    const totalValue = data[0].value + data[1].value;
    return (
      <g transform="translate(460,220)">
        <g>
          <circle r="5" cx="-10" cy="-5" fill="#D1CAED" />
          <text x="10" y="0" fontSize="12">
            {`${data[0].value} ( ${(data[0].value / totalValue) * 100} % )`}
          </text>
        </g>
        <g>
          <circle r="5" cx="-10" cy="15" fill="rgba(66, 47, 138, 0.87)" />
          <text x="10" y="20" fontSize="12">
            {`${data[1].value} ( ${(data[1].value / totalValue) * 100} % )`}
          </text>
        </g>
      </g>
    );
  };

  render() {
    const { data } = this.props;
    return (
      <>
        <Grid
          container
          item
          justify="center"
          style={{
            fontSize: "16px", //width < 1200 ? "10px" :
            fontWeight: "600",
          }}
        >
          {"Construction Services - Total Amount"}
        </Grid>

        <ResponsiveContainer width="100%" height="100%">
          <PieChart
            width={150}
            height={80}
            margin={{
              top: 10,
              left: -10,
              right: 10,
              bottom: 0,
            }}
          >
            <Legend verticalAlign="top" content={this.renderCustomLegends()} />

            <Pie
              data={data}
              cx="40%"
              cy="50%"
              outerRadius={120}
              dataKey="value"
            >
              {data.map((data, index) => (
                <Cell
                  key={`cell-${data.name}`}
                  fill={alternateColor[index % 2]}
                />
              ))}
            </Pie>
            {this.renderValue()}
          </PieChart>
        </ResponsiveContainer>
      </>
    );
  }
}

export default BigPieChart;
