import React, { Component } from "react";

import { Grid, withStyles, Typography } from "@material-ui/core";

import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Legend,
  Tooltip,
  ResponsiveContainer,
} from "recharts";
import millify from "millify";

const alternateColor = ["#D1CAED", "rgba(66, 47, 138, 0.87)"];

const CustomizedAxisTick = (props) => {
  //component
  const { x, y, payload } = props;
  const { innerWidth: width, innerHeight: height } = window;

  const [firstStr, secondStr] = payload.value.includes("-")
    ? payload.value.split("-")
    : payload.value.split(" ");

  return (
    <g transform={`translate(${x},${y})`} height={50} width={100}>
      <text
        dy={10}
        fontSize={width < 1200 ? 8 : 10}
        textAnchor="middle"
        fill="#666"
      >
        {firstStr}
      </text>
      <text
        dy={20}
        fontSize={width < 1200 ? 8 : 10}
        textAnchor="middle"
        fill="#666"
      >
        {secondStr}
      </text>
    </g>
  );
};

class BigBarCharts extends Component {
  state = {};

  getTheHighestValue = (data) => {
    let highestNumber = [];

    data.map((obj) => {
      highestNumber.push(obj.value);
    });

    return Math.max(...highestNumber);
  };

  constructionGraph = (data, isTotalData) => {
    const getAddNum = this.getTheHighestValue(data) / 4;

    const maxValue =
      Math.round((this.getTheHighestValue(data) + getAddNum) / 1000) * 1000;

    const maxValueTwo =
      Math.round((this.getTheHighestValue(data) + getAddNum) / 10) * 10;

    return (
      <>
        <ResponsiveContainer width="100%" height="100%">
          <BarChart
            width={150}
            height={80}
            data={data}
            margin={{
              top: 10,
              left: -30,
              right: 10,
              bottom: 0,
            }}
            style={{ cursor: "pointer" }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis
              dataKey="name"
              tickCount={6}
              interval={0}
              minTickGap={0}
              tick={<CustomizedAxisTick />}
              height={50}
            />
            <YAxis
              tickCount={isTotalData ? 6 : 8} //no. of levels in y axis
              interval={0}
              tick={{ fontSize: 8 }}
              type="number"
              // domain={[0, maxValueTwo]} //range of y axis
              domain={isTotalData ? [0, maxValue] : [0, maxValueTwo]} //range of y axis
              tickFormatter={(value) => this.dataYFormat(value)}
            />

            <Legend verticalAlign="top" content={this.renderCustomLegends()} />
            <Bar barSize={50} dataKey="value" radius={[10, 10, 0, 0]}>
              {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={alternateColor[index % 2]} /> //color alternation
              ))}
            </Bar>
          </BarChart>
        </ResponsiveContainer>
      </>
    );
  };

  dataYFormat = (value) => {
    //
    console.log("graph", value);

    if (isFinite(value)) {
      return `${millify(value)}`;
    }
    // return value;
  };

  budgetBreakDownGraph = (data) => {
    const getAddNum = this.getTheHighestValue(data) / 4;

    const maxValue =
      Math.round((this.getTheHighestValue(data) + getAddNum) / 1000) * 1000;

    return (
      <>
        <ResponsiveContainer width="100%" height="100%">
          <BarChart
            width={150}
            height={80}
            data={data}
            margin={{
              top: 10,
              left: -10,
              right: 10,
              bottom: 0,
            }}
            style={{ cursor: "pointer" }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis
              dataKey="name"
              tickCount={3}
              interval={0}
              minTickGap={0}
              tick={<CustomizedAxisTick />}
              height={50}
            />
            <YAxis
              tickCount={5}
              interval={0}
              tick={{ fontSize: 10 }}
              type="number"
              domain={[0, maxValue]}
              tickFormatter={(value) => this.dataYFormat(value)}
            />

            <Tooltip />
            <Bar
              barSize={50}
              dataKey="value"
              fill="rgba(66, 47, 138, 0.87)"
              radius={[15, 15, 0, 0]}
            />
          </BarChart>
        </ResponsiveContainer>
      </>
    );
  };

  renderCustomLegends = (api) => {
    return (
      <Grid
        container
        item
        justify="flex-end"
        md={12}
        style={{ marginBottom: 5 }}
      >
        <Grid
          container
          item
          alignItems="center"
          md={4}
          style={{ fontSize: "10px" }}
        >
          <Grid
            item
            style={{
              height: 10,
              width: 10,
              backgroundColor: "#D1CAED",
              borderRadius: "10px",
              marginRight: "5px",
            }}
          />
          <span>Economical</span>
        </Grid>

        <Grid
          container
          item
          alignItems="center"
          md={4}
          style={{ fontSize: "10px" }}
        >
          <Grid
            item
            style={{
              height: 10,
              width: 10,
              backgroundColor: "rgba(66, 47, 138, 0.87)",
              borderRadius: "10px",
              marginRight: "5px",
            }}
          />
          <span>Standard</span>
        </Grid>
      </Grid>
    );
  };
  render() {
    const { data, type, title } = this.props;
    return (
      <>
        <Grid
          container
          item
          justify="center"
          style={{
            fontSize: "16px", //width < 1200 ? "10px" :
            fontWeight: "600",
          }}
        >
          {title}
        </Grid>

        {type === 1 && this.budgetBreakDownGraph(data)}
        {type === 2 && this.constructionGraph(data, false)}
        {type === 3 && this.constructionGraph(data, true)}
      </>
    );
  }
}

export default BigBarCharts;
