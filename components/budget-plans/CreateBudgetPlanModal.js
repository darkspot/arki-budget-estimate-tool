import React, { Component } from "react";
import _ from "lodash";

//next
import { withRouter } from "next/router";

//components

//MUI
import { Button, Grid, TextField } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import MenuItem from "@material-ui/core/MenuItem";

//redux & actions
import { connect } from "react-redux";
import * as budgetPlanActions from "../../redux/actions/budget-plans";

// Final Form
import { Form, Field, FormSpy } from "react-final-form";
import arrayMutators from "final-form-arrays";
import { FieldArray } from "react-final-form-arrays";
import NumberFormat from "react-number-format";

class CreateBudgetPlanModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit = async (values) => {
    let newBudgetPlanData;
    let currentLocation = values.location;
    let generateCode = currentLocation.substring(0, 3).toLowerCase();
    newBudgetPlanData = { ...values, code: generateCode };

    await this.props.postCreatedBudget(newBudgetPlanData);
    await this.props.handleOpenModal(false);
    await this.props.getBudgetPlans();
  };

  closeModal = (data) => {
    this.props.handleOpenModal(data);
  };

  componentDidMount = async () => {
    await this.props.getCountries();
  };

  renderForms = (formProps) => {
    const { handleSubmit } = formProps;
    const { country_list } = this.props;
    console.log(country_list);
    return (
      <form onSubmit={handleSubmit}>
        <FormSpy
          subscription={{ values: true }}
          onChange={(formValue) =>
            console.log("This is form values", formValue.values)
          }
        />
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <p className="dl-component-description">Country</p>
            <Field
              name="country"
              render={({ meta, ...fieldProps }) => (
                <div>
                  <Select
                    className="dl-component-select"
                    value={fieldProps.input.value}
                    onChange={(event) => fieldProps.input.onChange(event)}
                    variant="outlined"
                    style={{ marginBottom: 25 }}
                    displayEmpty
                    renderValue={(value) => value || "Select Country"}
                  >
                    {country_list.map((country, index) => (
                      <MenuItem key={index} value={country.name}>
                        {country.name}
                      </MenuItem>
                    ))}
                  </Select>
                </div>
              )}
            />
            <p className="dl-component-description">City</p>
            <Field
              name="location"
              render={({ meta, ...fieldProps }) => (
                <div>
                  <OutlinedInput
                    value={fieldProps.input.value}
                    onChange={(event) => fieldProps.input.onChange(event)}
                    placeholder="Input City"
                    style={{ marginBottom: 25 }}
                    className="bp-component-city-filter-bar"
                  />
                </div>
              )}
            />
            <p className="dl-component-description">Currency</p>
            <Field
              name="currency"
              render={({ meta, ...fieldProps }) => (
                <div>
                  <OutlinedInput
                    value={fieldProps.input.value}
                    onChange={(event) => fieldProps.input.onChange(event)}
                    placeholder="Input Currency (e.g. Php)"
                    className="bp-component-city-filter-bar"
                  />
                </div>
              )}
            />
          </Grid>

          <Grid item xs={6} className="modal-price-range-maxHeight">
            <p className="dl-component-description">
              Typical Floor Price Range (sqft)
            </p>
            <Grid container spacing={2}>
              <Grid item xs={5}>
                <Field
                  name="lowest"
                  render={({ meta, ...fieldProps }) => (
                    <div>
                      <NumberFormat
                        value={fieldProps.input.value}
                        customInput={TextField}
                        min={0}
                        suffix=" sqft"
                        variant="outlined"
                        placeholder="MIN"
                        style={{ marginBottom: 19 }}
                        className="bp-component-city-filter-bar"
                        thousandSeparator
                        onValueChange={({ value: v }) =>
                          fieldProps.input.onChange({
                            target: { name, value: v },
                          })
                        }
                        inputProps={{
                          style: { textAlign: "right" },
                        }}
                      />
                    </div>
                  )}
                />
              </Grid>
              <Grid item xs={1}>
                <p>to</p>
              </Grid>
              <Grid item xs={5}>
                <Field
                  name="secondLowest"
                  render={({ meta, ...fieldProps }) => (
                    <div>
                      <NumberFormat
                        value={fieldProps.input.value}
                        customInput={TextField}
                        min={_.get(formProps.values, `secondLowest`) + 1}
                        suffix=" sqft"
                        variant="outlined"
                        placeholder="MAX"
                        style={{ marginBottom: 19 }}
                        className="bp-component-city-filter-bar"
                        thousandSeparator
                        onValueChange={({ value: v }) =>
                          fieldProps.input.onChange({
                            target: { name, value: v },
                          })
                        }
                        inputProps={{
                          style: { textAlign: "right" },
                        }}
                      />
                    </div>
                  )}
                />
              </Grid>
            </Grid>
            <FieldArray name="area_ranges_array">
              {({ fields }) => (
                <div>
                  {fields.map((name) => (
                    <div key={name}>
                      <Grid container spacing={2}>
                        <Grid item xs={5}>
                          <Field
                            name={`${name}.min`}
                            render={({ meta, ...fieldProps }) => (
                              <div>
                                <NumberFormat
                                  value={fieldProps.input.value}
                                  customInput={TextField}
                                  min={0}
                                  suffix=" sqft"
                                  variant="outlined"
                                  placeholder="MIN"
                                  style={{ marginBottom: 19 }}
                                  className="bp-component-city-filter-bar"
                                  thousandSeparator
                                  onValueChange={({ value: v }) =>
                                    fieldProps.input.onChange({
                                      target: { name, value: v },
                                    })
                                  }
                                  inputProps={{
                                    style: { textAlign: "right" },
                                  }}
                                />
                              </div>
                            )}
                          />
                        </Grid>
                        <Grid item xs={1}>
                          <p>to</p>
                        </Grid>
                        <Grid item xs={5}>
                          <Field
                            name={`${name}.max`}
                            render={({ meta, ...fieldProps }) => (
                              <div>
                                <NumberFormat
                                  value={fieldProps.input.value}
                                  customInput={TextField}
                                  min={
                                    _.get(formProps.values, `${name}.min`) + 1
                                  }
                                  suffix=" sqft"
                                  variant="outlined"
                                  placeholder="MAX"
                                  style={{ marginBottom: 19 }}
                                  className="bp-component-city-filter-bar"
                                  thousandSeparator
                                  onValueChange={({ value: v }) =>
                                    fieldProps.input.onChange({
                                      target: { name, value: v },
                                    })
                                  }
                                  inputProps={{
                                    style: { textAlign: "right" },
                                  }}
                                />
                              </div>
                            )}
                          />
                        </Grid>
                      </Grid>
                      {/* <button type="button" onClick={() => fields.remove(index)}>
                           Remove
                         </button> */}
                    </div>
                  ))}
                  <Button
                    type="button"
                    variant="contained"
                    className="modal-add-button"
                    disableElevation
                    onClick={() => fields.push({ min: "", max: "" })}
                  >
                    Add Another Price Range
                  </Button>
                </div>
              )}
            </FieldArray>
          </Grid>
        </Grid>
        <div className="bp-component-display-flex-end">
          <Button
            variant="contained"
            type="button"
            onClick={() => this.closeModal(false)}
            className="modal-cancel-button"
            disableElevation
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            type="submit"
            className="modal-submit-button"
            disableElevation
          >
            Done
          </Button>
        </div>
      </form>
    );
  };

  render() {
    const { budget_modal } = this.props;
    return (
      <Modal
        open={budget_modal}
        onClose={() => this.closeModal(false)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div className="centered-modal modal-style">
          <h2>Capex Rate Matrix</h2>
          <div className="modal-form-style">
            <Form
              onSubmit={this.handleSubmit}
              mutators={{
                ...arrayMutators,
              }}
              render={(formProps) => this.renderForms(formProps)}
            />
          </div>
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => ({
  budget_modal: state.budgetPlans.budget_plan_modal,
  country_list: state.budgetPlans.budget_plan_get_countries,
});

const mapDispatchToProps = (dispatch) => ({
  sampleGet: async () => {
    return dispatch(budgetPlanActions.sampleGetData());
  },
  handleOpenModal: async (bool) => {
    return dispatch(budgetPlanActions.handleOpenModal(bool));
  },
  postCreatedBudget: async (data) => {
    return dispatch(budgetPlanActions.postCreatedBudget(data));
  },
  getCountries: async () => {
    return dispatch(budgetPlanActions.getCountries());
  },
  getBudgetPlans: async () => {
    return dispatch(budgetPlanActions.getBudgetPlans());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CreateBudgetPlanModal));
