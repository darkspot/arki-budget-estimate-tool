import React from "react";
import { connect } from "react-redux";
import { withSnackbar } from "notistack";
import { Tab, Tabs } from "@material-ui/core";

import * as BudgetPlanActions from "../../redux/actions/budget-plans";

class BudgetPlanTab extends React.Component {
  handleChangePlan = (event, value) => {
    const { setCurrentPlan } = this.props;
    setCurrentPlan(value);
  };

  render() {
    const { budgetPlans, currentPlan } = this.props;
    // console.log("this current plan " + currentPlan);
    return (
      <Tabs
        color="primary"
        value={currentPlan}
        aria-label="disabled tabs example"
        onChange={this.handleChangePlan}
      >
        {budgetPlans.map((plan, index) => (
          <Tab key={index} label={plan.location}
            disabled={this.props.onEdit && currentPlan.id !== plan.id}
            value={plan} active={"true"} />
        ))}
      </Tabs>
    );
  }
}

const mapStateToProps = (state) => ({
  budgetPlans: state.budgetPlans.list,
  currentPlan: state.budgetPlans.currentPlan,
  onEdit: state.budgetPlans.onEdit,
});

const mapDispatchToProps = (dispatch) => ({
  setCurrentPlan: async (plan) => {
    return await dispatch(BudgetPlanActions.setCurrentPlan(plan));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withSnackbar(BudgetPlanTab));
