import React, { Component } from "react";
//Mui
import { Grid, Typography, Button, withStyles } from "@material-ui/core";

//components
import BudgetSearch from "./BudgetSearch";

//styles
import { budgetSearchStyles } from "../../styles/budget";
import BudgetTable from "./BudgetTable";

class Budget extends Component {
  state = {};
  render() {
    const { classes } = this.props;
    return (
      <>
        <Grid container alignItems="center" spacing={3}>
          <Grid item className={classes.title}>
            <Typography variant="h1">Budgets</Typography>
          </Grid>
          <Grid item>
            <Button variant="contained" color="secondary">
              Add Budget Plan
            </Button>
          </Grid>
        </Grid>
        <BudgetSearch></BudgetSearch>
        <BudgetTable></BudgetTable>
      </>
    );
  }
}

export default withStyles(budgetSearchStyles)(Budget);
