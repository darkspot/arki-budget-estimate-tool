import React, { Component } from "react";
//MUI
import { withStyles } from "@material-ui/core/styles";
import {
  Button,
  Grid,
  Typography,
  Select,
  MenuItem,
  OutlinedInput,
} from "@material-ui/core";
//components
//redux
//styles
import { budgetSearchStyles } from "../../styles/budget";

class BudgetSearch extends Component {
  state = {};

  handleChange = (event) => {
    if (event.target.name == "country") {
      this.setState({ country: event.target.value });
    } else if (event.target.name == "city") {
      this.setState({ city: event.target.value });
    }

    // console.log(event);
  };

  handleClickShowRate = () => {
    // console.log("clicked!");
  };

  render() {
    const { classes } = this.props;

    return (
      <>
        <Grid
          container
          item
          className={classes.searchHolder}
          justify="space-evenly"
        >
          <Grid container item md={5} direction="column">
            <Typography variant="h6">What are you looking for?</Typography>
            <OutlinedInput
              label="Search"
              // variant="outlined"
              placeholder="Search for countery, city .etc"
            ></OutlinedInput>
          </Grid>

          <Grid container item md={2} direction="column">
            <Typography variant="h6">Country</Typography>
            <Select
              name="country"
              variant="outlined"
              value={this.state.country || ""}
              onChange={(event) => this.handleChange(event)}
              //below is placeholder for select component
              displayEmpty
              renderValue={(value) => value || "Country"}
              style={{
                color: this.state.country ? "black" : "#bbb", //grey hex
              }}
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </Grid>
          <Grid container item md={2} direction="column">
            <Typography variant="h6">City</Typography>
            <Select
              name="city"
              variant="outlined"
              value={this.state.city || ""}
              onChange={(event) => this.handleChange(event)}
              //below is placeholder for select component
              displayEmpty
              renderValue={(value) => value || "City"}
              style={{
                color: this.state.city ? "black" : "#bbb", //grey hex
              }}
            >
              <MenuItem value={10}>Ten</MenuItem>
              <MenuItem value={20}>Twenty</MenuItem>
              <MenuItem value={30}>Thirty</MenuItem>
            </Select>
          </Grid>

          <Grid container item md={2} direction="column" justify="flex-end">
            <Button
              className={classes.showRatesBtn}
              onClick={this.handleClickShowRate}
            >
              Show Rates
            </Button>
          </Grid>
        </Grid>
      </>
    );
  }
}

export default withStyles(budgetSearchStyles)(BudgetSearch);
