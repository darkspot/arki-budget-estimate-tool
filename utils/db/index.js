import admin from "firebase-admin";
import serviceAccount from "../serviceAccountKey.json";
import serviceDevAccountKey from "../serviceDevAccountKey.json";

if (!admin.apps.length) {
  const service =
    process.env.NEXT_PUBLIC_FIRE_BASE_ENVIRONMENT === "development"
      ? serviceDevAccountKey
      : serviceAccount;

  try {
    admin.initializeApp({
      credential: admin.credential.cert(service),
    });
    console.log("Firebase Initialize");
  } catch (error) {
    console.log("Firebase admin initialization error", error.stack);
  }
}
export default admin.firestore();
