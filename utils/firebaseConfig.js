import firebase from "firebase";
import "firebase/auth";

// Your web app's Firebase configuration
// Put your SDK details

if (firebase.apps.length === 0) {
  var config;

  if (process.env.NEXT_PUBLIC_FIRE_BASE_ENVIRONMENT === "development") {
    console.log("development environment");
    // firebase.initializeApp(firebaseDevelopmentConfig);
    config = {
      apiKey: "AIzaSyDkblK7jZDsyl8ukPeUutxjy6UWIObi-IU",
      authDomain: "arki-budget-estimate-tool.firebaseapp.com",
      projectId: "arki-budget-estimate-tool",
      storageBucket: "arki-budget-estimate-tool.appspot.com",
      messagingSenderId: "142955501301",
      appId: "1:142955501301:web:4552bf128510082954d7bc",
      measurementId: "G-5ZSPS9T7LS",
    };
  }
  if (process.env.NEXT_PUBLIC_FIRE_BASE_ENVIRONMENT === "production") {
    console.log("production environment");
    // firebase.initializeApp(firebaseProductionConfig);
    config = {
      apiKey: "AIzaSyD9opaPISo2Dxj07I9tcQPnhmKxkA8qccA",
      authDomain: "arki-bet-dev.firebaseapp.com",
      projectId: "arki-bet-dev",
      storageBucket: "arki-bet-dev.appspot.com",
      messagingSenderId: "635512840452",
      appId: "1:635512840452:web:ee5ddb982edbb7cfd33051",
    };
  }

  firebase.initializeApp(config);
}

export default firebase;

// Initialize Provider & Export
export const microsoftProvider = new firebase.auth.OAuthProvider(
  "microsoft.com"
).setCustomParameters({
  prompt: "consent",
  // login_hint: "aronkev@gmail.com",
});
