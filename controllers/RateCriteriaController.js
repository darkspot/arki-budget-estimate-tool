import db from "../utils/db";

const collection = db.collection("rate_criterias");

export async function getAvailableRateCriterias(res) {
  try {
    const allEntries = await collection.get();
    const jsonifiedDocuments = allEntries.docs
      .reverse()
      .map((doc) => ({ ...doc.data(), id: doc.id }));
    res.status(200).json(jsonifiedDocuments);
  } catch (e) {
    res.status(400).end();
  }
}