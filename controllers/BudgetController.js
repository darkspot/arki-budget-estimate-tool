import db from "../utils/db";
import { budgetPlanCollection } from "./BudgetPlanController";
export const budgetCollection = db.collection("budget_list");

export async function getBudgetList(res) {
  try {
    const allEntries = await budgetCollection.get(); // Get all the data in firebase based on Data Set
    const jsonifiedDocuments = allEntries.docs
      .reverse()
      .map((doc) => ({ ...doc.data(), id: doc.id }));
    res.status(200).json(jsonifiedDocuments);
  } catch (e) {
    res.status(400).end();
  }
}

export async function getBudgetById(req, res) {
  try {
    const budgetId = req.query.budgetId;
    const budgetDocument = await budgetCollection.doc(budgetId).get();

    if (!budgetDocument.exists) {
      throw new Error('Budget do not exists');
    }

    res.status(200).json({ ...budgetDocument.data(), id: budgetDocument.id });
  } catch (e) {
    res.status(400).end();
  }
}

export async function createBudget(req, res) {
  try {
    const budgetPlanId = req.body.budget.budget_plan;
    const budgetPlan = await (await budgetPlanCollection.doc(budgetPlanId).get()).data();
    const document = await budgetCollection.add({
      ...req.body.budget,
      location: budgetPlan.location,
      currency: budgetPlan.currency,
    });
    res.status(200).json(document);
  } catch (e) {
    console.log(e);
    res.status(400).json({ message: `Cannot Create Budget` });
  }
}

export async function updateBudget(req, res) {
  console.log("update method treigged!");
  try {
    const budget = req.body.budget;
    const budgetPlanId = req.body.budget.budget_plan;
    const budgetPlan = await (await budgetPlanCollection.doc(budgetPlanId).get()).data();
    await budgetCollection.doc(budget.id).update({
      ...budget,
      location: budgetPlan.location,
      currency: budgetPlan.currency,
    });
    res.status(200).json("update success");
  } catch (e) {
    console.log(e);
    res.status(400).end();
  }
}

export async function deleteBudget(req, res) {
  console.log("Delete Method Triggered");
  try {
    await budgetCollection.doc(`${req.query.budgetId}`).delete();
    res.status(200).json("delete success");
  } catch (e) {
    res.status(400).end();
    console.log("Error: " + e);
  }
}