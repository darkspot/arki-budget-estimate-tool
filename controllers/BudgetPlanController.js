import db from "../utils/db";
import _ from "lodash";

export const budgetPlanCollection = db.collection("budget_plans");

export async function getBudgetPlans(res) {
  try {
    const allEntries = await budgetPlanCollection.get();
    const jsonifiedDocuments = allEntries.docs
      .reverse()
      .map((doc) => ({ ...doc.data(), id: doc.id }));
    res.status(200).json(jsonifiedDocuments);
  } catch (e) {
    console.log("Error: ", e);
    res.status(400).end();
  }
}

export async function getBudgetPlanById(req, res) {
  try {
    const planId = req.query.planId;
    const document = await budgetPlanCollection.doc(planId).get();

    if (!document.exists) {
      throw new Error("Budget plan do not exists");
    }

    res.status(200).json({ ...document.data(), id: document.id });
  } catch (e) {
    res.status(400).end();
  }
}

export async function createBudgetPlan(req, res) {
  try {
    const currency = _.toUpper(req.body?.currency);
    const document = await budgetPlanCollection.add({ ...req.body, currency });
    res.status(200).json(document);
  } catch (e) {
    console.log(e);
    res.status(400).json({ message: `Cannot Create Budget` });
  }
}

export async function updateBudget(req, res) {
  console.log("update method treigged!");
  try {
    await budgetPlanCollection.doc(`${req.body.location}`).update({
      rates: req.body.rates,
    });

    res.status(200).json("update success");
  } catch (e) {
    res.status(400).end();
    console.log("Error: " + e);
  }
}

export async function deleteBudget(req, res) {
  console.log("Delete Method Triggered");
  try {
    await budgetPlanCollection.doc(`${req.body.location}`).update({
      rates: req.body.rates,
    });

    res.status(200).json("update success");
  } catch (e) {
    res.status(400).end();
    console.log("Error: " + e);
  }
}

export async function getAvailableLocations(res) {
  console.log("Get Available Locations");
  try {
    const allEntries = await budgetPlanCollection
      .select("id", "location", "code")
      .get();
    const jsonifiedDocuments = allEntries.docs
      .reverse()
      .map((doc) => ({ ...doc.data(), id: doc.id }));
    res.status(200).json(jsonifiedDocuments);
  } catch (e) {
    res.status(400).end();
  }
}
